package com.catcoin.tool.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
    /**
     * 交易所日志
     * @return
     */
    @Bean
    public Queue  testQueue(){
        return new Queue("loghhtbe");
    }

    /**
     * 猫币App操作日志队列
     * @return
     */
    @Bean
    public Queue catCoinQueue(){
        return new Queue("catCoin");
    }
}
