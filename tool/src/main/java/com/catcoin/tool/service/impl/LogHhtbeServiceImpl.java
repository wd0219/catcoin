package com.catcoin.tool.service.impl;

import com.catcoin.tool.model.LogHhtbe;
import com.catcoin.tool.mapper.LogHhtbeMapper;
import com.catcoin.tool.service.ILogHhtbeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * Hhtb流水 服务实现类
 * </p>
 *
 * @author cdy
 * @since 2018-09-14
 */
@Service
public class LogHhtbeServiceImpl extends ServiceImpl<LogHhtbeMapper, LogHhtbe> implements ILogHhtbeService {

}
