package com.catcoin.tool.service;

import java.sql.Timestamp;
import java.util.Date;

public interface IToolService {

    /**
     *获取用户余额
     * @param userID
     * @param coinID 币种ID: 21=HHTB,17=MAOC
     * @param companyID 公司Id
     * @param md5info
     * @return
     */
    public Object getBalance(String userID,String coinID,String companyID,String md5info);

    /**
     * 获取用户账单
     * @param userID
     * @param coinID 币种ID: 21=HHTB,17=MAOC
     * @param companyID 公司Id
     * @param page
     * @param md5info
     * @return
     */
    public Object getFunds(String userID,String coinID,String companyID,String page,String md5info);

    /**
     * 用户冲币扣币
     * @param userID
     * @param coinID  币种ID: 21=HHTB,17=MAOC
     * @param isBuy 1=充币，0=扣币
     * @param amount 金额
     * @param requestTime  申请时间
     * @param companyID 公司Id
     * @param md5Info
     * @return
     */
    public Object operate(String userID, String coinID, String isBuy, String amount, String requestTime, String companyID, String md5Info);

    /**
     * 获取用户id
     * @param companyID 公司Id
     * @param userMobile 用户手机号
     * @param md5Info
     * @return
     */
    public Object  register(String companyID,String userMobile,String md5Info);

    /**
     * 获取人民币转猫币利率
     * @return
     */
    public Object ticker();

    /**
     * 发送短信的接口
     * @param mobile
     * @param content
     * @return
     */
    public Object sendSMS(String mobile,String content);

}
