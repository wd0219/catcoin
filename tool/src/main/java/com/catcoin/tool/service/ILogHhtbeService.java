package com.catcoin.tool.service;

import com.catcoin.tool.model.LogHhtbe;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * Hhtb流水 服务类
 * </p>
 *
 * @author cdy
 * @since 2018-09-14
 */
public interface ILogHhtbeService extends IService<LogHhtbe> {

}
