package com.catcoin.tool.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.catcoin.tool.service.IToolService;
import com.catcoin.tool.util.HttpUtil;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class ToolServiceimpl implements IToolService {


    @Override
    public Object getBalance(String userID, String coinID, String companyID, String md5info) {
        Map<String,Object> map = new HashMap<>();
        map.put("userID",userID);
        map.put("coinID",coinID);
        map.put("companyID",companyID);
        map.put("md5info",md5info);
        String url = "https://api1.digsg.com/assets/balance";
        return HttpUtil.doPost(url,map);
//        return null;
    }

    @Override
    public Object getFunds(String userID, String coinID, String companyID, String page, String md5Info) {
        Map<String,Object> map = new HashMap<>();
        map.put("userID",userID);
        map.put("coinID",coinID);
        map.put("companyID",companyID);
        map.put("page",page);
        map.put("md5info",md5Info);
        String url = "https://api1.digsg.com/funds";
        return HttpUtil.doPost(url,map);
    }

    @Override
    public Object operate(String userID, String coinID, String isBuy, String amount, String requestTime, String companyID, String md5Info) {
        Map<String,Object> map = new HashMap<>();
        map.put("userID",userID);
        map.put("coinID",coinID);
        map.put("isBuy",isBuy);
        map.put("amount",amount);
        map.put("requestTime",requestTime);
        map.put("companyID",companyID);
        map.put("md5info",md5Info);
        String url = "https://api1.digsg.com/cooperation/operate";
        return HttpUtil.doPost(url,map);
    }

    @Override
    public Object register(String companyID, String userMobile, String md5Info) {
        Map<String,Object> map = new HashMap<>();
        map.put("companyID",companyID);
        map.put("userMobile",userMobile);
        map.put("md5Info",md5Info);
        String url = "https://api1.digsg.com/user/register";
        return HttpUtil.doPost(url,map);
    }

    @Override
    public Object ticker() {
        String url = "https://api1.digsg.com/ticker/ticker?market=maoc_hhtb";
        String json =  HttpUtil.doGet(url);

        JSONObject jsonObject  = JSONObject.parseObject(json);
        JSONObject datas = jsonObject.getJSONObject("datas");
        String cnyPrices = datas.getString("cnyPrice");
        double cnyPrice = 1 / Double.valueOf(cnyPrices);
        return cnyPrice;
    }

    @Override
    public Object sendSMS(String mobile, String content) {
        Map<String,Object> map = new HashMap<>();
        map.put("CorpID","hbky007093");
        map.put("Pwd","zm0513@");
        map.put("Mobile",mobile);
        map.put("Content",content);
        String url = "https://sdk2.028lk.com/sdk2/BatchSend2.aspx";
        return HttpUtil.doPost(url,map);
    }


}
