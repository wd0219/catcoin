package com.catcoin.tool.controller;

import com.catcoin.tool.util.HttpUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @RequestMapping("/getTest")
    public Object getTest(@RequestParam String name){
        return "name:"+name;
    }

    @RequestMapping("/test")
    public Object test(){
        return "test";
    }

    @RequestMapping("/test1")
    public Object test1(){
        return "test1";
    }

    @RequestMapping("/test2")
    public Object test2(){
        return "test2";
    }

    @RequestMapping("/test3")
    public Object test3(@RequestParam String url){
//        String url = "https://www.sojson.com/open/api/weather/json.shtml?city=北京";
        String json = HttpUtil.doGet(url).toString();
        return json;
    }
}
