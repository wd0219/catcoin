package com.catcoin.tool.controller;


import com.catcoin.tool.service.IToolService;
import com.catcoin.tool.util.MD5Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@Slf4j
public class ToolController {

    @Autowired
    private IToolService toolService;

    /**
     * 获取用户余额
     * @param userID 交易所用户id
     * @param coinID 币种  21=HHTB,17=MAOC猫币
     * @return
     */
    @RequestMapping("/getBalance")
    public Object getBalance(String userID, String coinID){
        //String userID = "294347";
        //String coinID = "21";
        String companyID = "1";
        String secretKey = "iL34.#dnZ2ubJJFt";
        String key = userID+coinID+companyID+secretKey;
        String md5info = MD5Util.encrypt(key);
        return toolService.getBalance(userID,coinID,companyID,md5info);
    }

    /**
     * 获取用户账单
     * @param userID
     * @param coinID 币种  21=HHTB,17=MAOC猫币
     * @param page
     * @return
     */
    @RequestMapping("/getFunds")
    public Object getFunds(String userID, String coinID,String page){
        //String userID = "294347";
        //String coinID = "21";
        String companyID = "1";
        //String page = "1";
        String secretKey = "iL34.#dnZ2ubJJFt";
        String key = userID+coinID+companyID+secretKey;
        String md5info = MD5Util.encrypt(key);
        return toolService.getFunds(userID,coinID,companyID,page,md5info);
    }

    /**
     * 获取交易所用户Id
     * @param userMobile
     * @return
     */
    @RequestMapping("/register")
    public Object  register(String userMobile){
        String companyID = "1";
        //String userMobile = "13400355529";
        String secretKey = "iL34.#dnZ2ubJJFt";
        String key = userMobile+companyID+secretKey;
        String md5Info = MD5Util.encrypt(key);
        return toolService.register(companyID,userMobile,md5Info);
    }

    /**
     * 用户冲币扣币
     * @param userID
     * @param coinID 币种  21=HHTB,17=MAOC猫币
     * @param isBuy 1=充币，0=扣币
     * @param amount 金额
     * @return
     */
    @RequestMapping("/operate")
    public Object operate(String userID,String coinID,String isBuy,String amount){
        //String userID = "305312";
        //String coinID = "17";
        //String isBuy = "0";
        //String amount = "1";
        String  requestTime = Long.toString(new Date().getTime()/1000);
        String companyID = "1";
        String secretKey = "iL34.#dnZ2ubJJFt";
        String key = userID+coinID+isBuy+amount+requestTime+companyID+secretKey;
        String md5Info = MD5Util.encrypt(key);
        return toolService.operate(userID,coinID,isBuy,amount,requestTime,companyID,md5Info);
    }

    @RequestMapping("/md5")
    public Object md5(String values){
        return MD5Util.encrypt(values);
    }

    /**
     * 获取人民币转猫币利率
     * @return
     */
    @RequestMapping("/ticker")
    public Object ticker(){
        return toolService.ticker();
    }

    /**
     * 验证码短信
     * 如果返回值大于0说明发送成功
     * @return
     */
    @RequestMapping("/verificationSMS")
    public Object verificationSMS(String mobile,String code){
        String content = "您的验证码为"+code+",该验证码5分钟内有效，请勿泄露与他人!【华猫APP】";
        return toolService.sendSMS(mobile,content);
    }

}
