package com.catcoin.tool.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Remote {

    @RequestMapping("/remote")
    public Object remote(){
        return "remote";
    }
}
