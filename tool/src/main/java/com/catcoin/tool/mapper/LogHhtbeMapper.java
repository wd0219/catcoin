package com.catcoin.tool.mapper;

import com.catcoin.tool.model.LogHhtbe;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * Hhtb流水 Mapper 接口
 * </p>
 *
 * @author cdy
 * @since 2018-09-14
 */
public interface LogHhtbeMapper extends BaseMapper<LogHhtbe> {

}
