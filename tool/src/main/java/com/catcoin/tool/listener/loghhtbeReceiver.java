package com.catcoin.tool.listener;

import com.alibaba.fastjson.JSONObject;
import com.catcoin.tool.model.LogHhtbe;
import com.catcoin.tool.service.ILogHhtbeService;
import com.catcoin.tool.service.IToolService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "loghhtbe")
public class loghhtbeReceiver {

    @Autowired
    private ILogHhtbeService logHhtbeService;

    @RabbitHandler
    public void process(String  context) {
        System.out.println("Receiver : " + context);
        JSONObject jsonObject = JSONObject.parseObject(context);
        LogHhtbe logHhtbe = (LogHhtbe) JSONObject.toJavaObject(jsonObject,LogHhtbe.class);
        logHhtbeService.insert(logHhtbe);
    }
}
