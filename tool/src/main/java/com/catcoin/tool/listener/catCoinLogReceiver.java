package com.catcoin.tool.listener;

import com.alibaba.fastjson.JSONObject;
import com.catcoin.tool.mongodb.CatCoinLog;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "catCoin")
public class catCoinLogReceiver {

    @Autowired
    private MongoTemplate mongoTemplate;


    @RabbitHandler
    public void process(String  context) {
        System.out.println("Receiver : " + context);
        JSONObject jsonObject = JSONObject.parseObject(context);
        CatCoinLog catCoinLog = (CatCoinLog) JSONObject.toJavaObject(jsonObject,CatCoinLog.class);
        mongoTemplate.save(catCoinLog);
    }
}
