package com.catcoin.consume.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author caody
 * @since 2018-08-31
 */
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "userId", type = IdType.AUTO)
    private Integer userId;
    /**
     * 用户名称
     */
    @TableField("userName")
    private String userName;
    /**
     * 用户手机号
     */
    @TableField("userPhone")
    private String userPhone;
    /**
     * 密码
     */
    @TableField("password")
    private String password;
    /**
     * 支付密码
     */
    @TableField("payPwd")
    private String payPwd;
    /**
     * 推荐人id
     */
    @TableField("inviteId")
    private Integer inviteId;
    /**
     * 用户类型 0普通 1主管 2经理
     */
    @TableField("userType")
    private Integer userType;
    /**
     * 头像
     */
    @TableField("userPhoto")
    private String userPhoto;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;
    /**
     * 账号状态：0停用 1启用
     */
    @TableField("userStatus")
    private Integer userStatus;
    /**
     * 是否允许转 0否 1 是
     */
    @TableField("isOperation")
    private Integer isOperation;
    /**
     * 盐
     */
    @TableField("salt")
    private Integer salt;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPayPwd() {
        return payPwd;
    }

    public void setPayPwd(String payPwd) {
        this.payPwd = payPwd;
    }

    public Integer getInviteId() {
        return inviteId;
    }

    public void setInviteId(Integer inviteId) {
        this.inviteId = inviteId;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Integer getIsOperation() {
        return isOperation;
    }

    public void setIsOperation(Integer isOperation) {
        this.isOperation = isOperation;
    }

    public Integer getSalt() {
        return salt;
    }

    public void setSalt(Integer salt) {
        this.salt = salt;
    }

    @Override
    protected Serializable pkVal() {
        return this.userId;
    }

    @Override
    public String toString() {
        return "User{" +
        "userId=" + userId +
        ", userName=" + userName +
        ", userPhone=" + userPhone +
        ", password=" + password +
        ", payPwd=" + payPwd +
        ", inviteId=" + inviteId +
        ", userType=" + userType +
        ", userPhoto=" + userPhoto +
        ", createTime=" + createTime +
        ", userStatus=" + userStatus +
        ", isOperation=" + isOperation +
        ", salt=" + salt +
        "}";
    }
}
