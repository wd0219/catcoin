package com.catcoin.consume.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * Hhtb流水
 * </p>
 *
 * @author gxz
 * @since 2018-09-12
 */
@TableName("log_hhtbe")
public class LogHhtbeDTO extends LogHhtbe {

    private String createtimes;

    public String getCreatetimes() {
        return createtimes;
    }

    public void setCreatetimes(String createtimes) {
        this.createtimes = createtimes;
    }
}
