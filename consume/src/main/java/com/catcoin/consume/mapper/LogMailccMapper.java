package com.catcoin.consume.mapper;

import com.catcoin.consume.model.LogMailcc;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户消费账户流水表 Mapper 接口
 * </p>
 *
 * @author consume
 * @since 2018-09-03
 */
public interface LogMailccMapper extends BaseMapper<LogMailcc> {

    List<Map<String,Object>> selectRecordByUserId(LogMailcc logMailcc);
    void add(LogMailcc logMailcc);

    List<Map<String,Object>> selectAllList(@Param(value = "userId")Integer userId);
}
