package com.catcoin.consume.mapper;

import com.catcoin.consume.model.Account;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.catcoin.consume.model.LogMailcc;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户账户表 Mapper 接口
 * </p>
 *
 * @author consume
 * @since 2018-09-03
 */
public interface AccountMapper extends BaseMapper<Account> {


    List<Map<String,Object>> selectAccountByUserId(Account account);
    Account selectByUserId(Integer userId);
}
