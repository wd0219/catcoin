package com.catcoin.consume.mapper;

import com.catcoin.consume.model.LogHhtbe;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * Hhtb流水 Mapper 接口
 * </p>
 *
 * @author gxz
 * @since 2018-09-12
 */
public interface LogHhtbeMapper extends BaseMapper<LogHhtbe> {

}
