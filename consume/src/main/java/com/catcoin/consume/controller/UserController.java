package com.catcoin.consume.controller;


import com.catcoin.consume.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author caody
 * @since 2018-08-31
 */
@RestController
public class UserController {
    @Autowired
    private IUserService userService;

    @RequestMapping("/getString")
    public Object getString(){
        return userService.selectById(1);
    }
}

