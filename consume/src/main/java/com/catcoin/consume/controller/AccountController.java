package com.catcoin.consume.controller;


import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.catcoin.consume.model.Account;
import com.catcoin.consume.result.AppResult;
import com.catcoin.consume.result.Result;
import com.catcoin.consume.service.IAccountService;
import com.catcoin.consume.service.ToolfeignInterface;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 用户账户表 前端控制器
 * </p>
 *
 * @author consume
 * @since 2018-09-03
 */
@Controller
@RequestMapping("/account")
@CrossOrigin
public class AccountController {


    @Autowired
    private IAccountService iAccountService;
    @Autowired
    private ToolfeignInterface toolfeignInterface;

    /**
     * 交易所猫币月转换消费余额
     * @param pageIndex
     * @return
     */
    @RequestMapping("/balance")
    @ResponseBody
    public Result balance(@ModelAttribute("pageIndex") String pageIndex, @ModelAttribute("userId")Integer userId){
        try {
            //String ticker = toolfeignInterface.ticker();
            //User user =(User) cacheUtil.get(SessionUtils.getSessionId());
            Account account = new Account();
            account.setUserId(userId);
            Integer pageNum=Integer.parseInt(pageIndex);
            Result results=iAccountService.balance(pageNum,account);
            return results;
        }catch (Exception e){
            e.printStackTrace();
            return Result.EEROR("您还没有登录");
        }
    }

    /**
     * 交易所猫币月转换消费余额  1111
     * @param userId 用户id
     * @return 成功ok
     */
    @RequestMapping("/getBalance")
    public AppResult getBalance(Integer userId, Integer pageNum){
        Integer pageSize = 10;
        PageHelper.startPage(pageNum,pageSize);
        List<Account> list = iAccountService.getAccount(userId);
        PageInfo<Account> info = new PageInfo<>(list);
        Integer totalPages = info.getPages();
        return  AppResult.OK(totalPages,"success",list);

    }

}

