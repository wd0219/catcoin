package com.catcoin.consume.controller;

import com.catcoin.consume.service.ToolfeignInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @Autowired
    private ToolfeignInterface toolfeignInterface;

    @Value("${server.port}")
    String port;

    @Value("${spring.application.name}")
    String serviceName;

    @RequestMapping("/")
    public String index(){
        return "serviceName=" + serviceName + "-------port=" + port;
    }


    @RequestMapping("/test1")
    public Object getTest(){
        return  toolfeignInterface.test1();
    }



}
