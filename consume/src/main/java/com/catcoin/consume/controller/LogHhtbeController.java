package com.catcoin.consume.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.catcoin.consume.model.LogHhtbe;
import com.catcoin.consume.model.LogHhtbeDTO;
import com.catcoin.consume.result.AppResult;
import com.catcoin.consume.service.ILogHhtbeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * Hhtb流水 前端控制器
 * </p>
 *
 * @author gxz
 * @since 2018-09-12
 */
@Controller
@CrossOrigin
public class LogHhtbeController {

    @Autowired
    private ILogHhtbeService logHhtbeService;

    /**
     *
     * 查询用户hhtb流水
     *
     * @param userId 用户ID
     * @param type 流水类型 0充值 1提现
     * @param pageNum 页数
     * @param pageSize 一页多少数据
     * @return
     */
    @RequestMapping("/getHhtbFlow")
    @ResponseBody
    public AppResult getHhtbFlow(String userId, String type,String pageNum,String pageSize){
        if(pageNum == null || "".equals(pageNum)){
            pageNum = "1";
        }
        if(pageSize == null || "".equals(pageSize)){
            pageSize = "10";
        }
        Integer pagenum = Integer.parseInt(pageNum);
        Integer pagesize = Integer.parseInt(pageSize);
        PageHelper.startPage(pagenum, pagesize);
        LogHhtbe logHhtbe = new LogHhtbe();
        logHhtbe.setUserId(Integer.parseInt(userId));
        if(type != null && !"".equals(type)){
            logHhtbe.setType(Integer.parseInt(type));
        }
        logHhtbe.setDataFlag(1);
        EntityWrapper<LogHhtbe> ew = new EntityWrapper<>(logHhtbe);
        ew.orderBy("createTime",false);
        PageHelper.startPage(pagenum, pagesize);
        List<LogHhtbe> list = logHhtbeService.selectList(ew);
        List<LogHhtbeDTO> list2 = new ArrayList<LogHhtbeDTO>();
        Integer totalPages = 1;
        if(list != null && list.size() > 0){
            PageInfo pageInfo=new PageInfo(list) ;
            totalPages = pageInfo.getPages();
            for (LogHhtbe lh:
                list ) {
                LogHhtbeDTO LogHhtbeDto=new LogHhtbeDTO();
                BeanUtils.copyProperties(lh,LogHhtbeDto);
                Date createTime = lh.getCreateTime();
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String format = dateFormat.format(createTime);
                LogHhtbeDto.setCreatetimes(format);
                list2.add(LogHhtbeDto);
            }
        }
        return  AppResult.OK(totalPages,"",list2);
    }

}

