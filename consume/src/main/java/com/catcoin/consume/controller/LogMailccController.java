package com.catcoin.consume.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.catcoin.consume.model.Account;
import com.catcoin.consume.model.LogMailcc;
import com.catcoin.consume.result.AppResult;
import com.catcoin.consume.service.IAccountService;
import com.catcoin.consume.service.ILogMailccService;
import com.catcoin.consume.service.ToolfeignInterface;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户消费账户流水表 前端控制器
 * </p>
 *
 * @author consume
 * @since 2018-09-03
 */
@RestController
@CrossOrigin
public class LogMailccController {
    @Autowired
    private ILogMailccService iLogMailccService;

    @Autowired
    private IAccountService iAccountService;

    @Autowired
    private ToolfeignInterface toolfeignInterface;




    /**
     *
     * 理财钱包消费 流水
     * @param userId 用户id
     * @return 成功ok
     */
    @RequestMapping("/logMailcc/getrecord")
    @ResponseBody
    public AppResult getrecord(Integer userId, Integer pageNum){
        Integer pageSize = 10;
        PageHelper.startPage(pageNum,pageSize);
        List<Map<String,Object>> list = iLogMailccService.getrecord(userId);
        PageInfo<Map<String,Object>> info = new PageInfo<>(list);
        Integer totalPages = info.getPages();
        return  AppResult.OK(totalPages,"success",list);

    }

    /**
     * 消费余额
     * @param userId  用户id
     * @return
     */
    @RequestMapping("/logMailcc/into")
    @ResponseBody
    public AppResult mailccinto(@ModelAttribute("userId") Integer userId){
        try {
            //从账户表中拿到理财余额
            Account account;
            account = iAccountService.selectByUserId(userId);
            if(account == null){
                return AppResult.ERROR("用户不存在！");
            }
            return AppResult.OK(account.getMailcc());
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("操作失败！");
        }
    }

    /**
     * 交易所猫币月转换消费余额
     * @param shiftToSum  变化的数量
     * @param userId  用户id
     * @return
     */
    @RequestMapping("/logMailcc/out")
    @ResponseBody
    public AppResult mailccout(@ModelAttribute("userId") Integer userId, @ModelAttribute("shiftToSum") String shiftToSum){
        try {

            //从账户表中拿到理财余额
            Account account;
            account = iAccountService.selectByUserId(userId);
            if(account == null){
                return AppResult.ERROR("用户不存在！");
            }
            Map<String,String> result = new HashMap<String,String>();
            //调用提币接口获取猫币余额总额
            BigDecimal total = new BigDecimal("0");
            //@param userID 交易所用户id     @param coinID 币种  21=HHTB,17=MAOC猫币
            String balance = toolfeignInterface.getBalance("305312", "17");
            if("getBalanceERROR".equals(balance)){
                return AppResult.ERROR("查询可用猫币余额失败");
            }
            JSONObject jsonObject = JSONObject.parseObject(balance);
            if("true".equals(jsonObject.get("isSuc").toString())){
                JSONObject jsonObject1 = JSONObject.parseObject(jsonObject.get("datas").toString());
                total = new BigDecimal(jsonObject1.get("available").toString());
            }

            BigDecimal shiftToSumTotal = new BigDecimal(shiftToSum);
            if (shiftToSumTotal.compareTo(total) == 1){
                return AppResult.ERROR("猫币余额不足");
            }
            //更新账户的余额
            account.setBlancecc(account.getBlancecc().subtract(total));
            iAccountService.updateById(account);

            //用户消费账户流水表对应添加记录
            LogMailcc logMailcc = new LogMailcc();
            logMailcc.setCreateTime(new Date());
            logMailcc.setUserId(userId);
            logMailcc.setFromId(userId);
            logMailcc.setOrderNo(com.catcoin.consume.util.StringUtils.getOrderIdByTime("7"));
            logMailcc.setPreMailcc(total);
            logMailcc.setMailcc(new BigDecimal(shiftToSum));
            logMailcc.setMailccType(-1);
            logMailcc.setRemark("猫币余额消费");
            logMailcc.setDataFlag(1);
            logMailcc.setType(1);
            iLogMailccService.insert(logMailcc);



            return AppResult.OK("猫币余额消费余额为："+account.getBlancecc());
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("操作失败！");
        }
    }


    /**
     * 提币后消费流水变更
     * @param mailcc  提币的数量
     * @param userId  用户id
     * @return
     */
    @RequestMapping("/logMailcc/intobalance")
    @ResponseBody
    public AppResult intobalance(@ModelAttribute("userId") Integer userId, @ModelAttribute("mailcc") BigDecimal mailcc){
        try {
            //从账户表中拿到理财余额
            Account account;
            account = iAccountService.selectByUserId(userId);
            if(account == null){
                return AppResult.ERROR("用户不存在！");
            }

            //用户消费账户流水表对应添加记录
            LogMailcc logMailcc = new LogMailcc();
            logMailcc.setCreateTime(new Date());
            logMailcc.setUserId(userId);
            logMailcc.setFromId(userId);
            logMailcc.setOrderNo(com.catcoin.consume.util.StringUtils.getOrderIdByTime("7"));
            logMailcc.setPreMailcc(account.getMailcc());
            logMailcc.setMailcc(mailcc);
            logMailcc.setMailccType(1);
            logMailcc.setRemark("提币消费");
            logMailcc.setDataFlag(1);
            logMailcc.setType(1);
            iLogMailccService.insert(logMailcc);
            //更新账户的余额
            account.setMailcc(account.getMailcc().add(mailcc));
            iAccountService.updateById(account);
            return AppResult.OK("提币消费后余额为："+account.getMailcc());
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("操作失败！");
        }
    }


}

