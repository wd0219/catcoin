package com.catcoin.consume.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    /**
     * 猫币App操作日志队列
     * @return
     */
    @Bean
    public Queue catCoinQueue(){
        return new Queue("catCoin");
    }

}
