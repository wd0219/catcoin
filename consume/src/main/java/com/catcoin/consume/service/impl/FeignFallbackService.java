package com.catcoin.consume.service.impl;

import com.catcoin.consume.service.ToolfeignInterface;
import org.springframework.stereotype.Service;

@Service
public class FeignFallbackService implements ToolfeignInterface {
    @Override
    public String test1() {
        return "ERROR";
    }

    @Override
    public Object operate(String userID, String coinID, String isBuy, String amount) {
        return null;
    }

    @Override
    public String ticker() {
        return "error1";
    }

    @Override
    public String getBalance(String userID, String coinID) {
        return "error";
    }


}
