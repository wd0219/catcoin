package com.catcoin.consume.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.catcoin.consume.model.Account;
import com.catcoin.consume.model.LogMailcc;
import com.catcoin.consume.mapper.LogMailccMapper;
import com.catcoin.consume.result.Result;
import com.catcoin.consume.service.ILogMailccService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 用户消费账户流水表 服务实现类
 * </p>
 *
 * @author consume
 * @since 2018-09-03
 */
@Service
public class LogMailccServiceImpl extends ServiceImpl<LogMailccMapper, LogMailcc> implements ILogMailccService {

    @Autowired
    private  LogMailccMapper logMailccMapper;

    @Override
    public Result record(Integer pageNum,LogMailcc logMailcc) {
        PageHelper.startPage(Integer.valueOf(pageNum),15);
        PageHelper.startPage(1, 10);
        List<Map<String,Object>> recordAlllist= new ArrayList<>();
        //设置分页
        //PageInfo<LogMailcc> pageInfo= new PageInfo<>(recordAlllist);
        recordAlllist=logMailccMapper.selectRecordByUserId(logMailcc);
        List<Object> list = new ArrayList<>();
        if (recordAlllist.size()>0) {
            for (Map<String,Object> recordMap : recordAlllist) {
                Map<String, Object> map = new HashMap<>();
                map.put("orderNo", recordMap.get("orderNo"));
                map.put("userId", recordMap.get("userId"));
                map.put("fromId", recordMap.get("fromId"));
                map.put("preMailcc", recordMap.get("preMailcc"));
                map.put("mailcc", recordMap.get("mailcc"));
                if (Integer.parseInt(recordMap.get("mailccType").toString())== -1) {
                    map.put("mailccType", "减少");
                }else {
                    map.put("mailccType", "增加");
                }
                map.put("remark", recordMap.get("remark"));
                if (Integer.parseInt(recordMap.get("dataFlag").toString())== 1) {
                    map.put("dataFlag", "有效");
                }else {
                    map.put("dataFlag", "删除");
                }
                map.put("type", recordMap.get("type"));
                map.put("createTime", recordMap.get("createTime"));
                list.add(map);
            }
        }
        return Result.OK(list,"查询成功!");
    }

    @Override
    public void add(LogMailcc logMailcc) {
        logMailccMapper.add(logMailcc);
    }

    @Override
    public List<Map<String,Object>> getrecord(Integer userId) {

        List<Map<String,Object>> list = logMailccMapper.selectAllList(userId);
        return  list;
    }
}
