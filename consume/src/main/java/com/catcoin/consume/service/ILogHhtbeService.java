package com.catcoin.consume.service;

import com.catcoin.consume.model.LogHhtbe;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * Hhtb流水 服务类
 * </p>
 *
 * @author gxz
 * @since 2018-09-12
 */
public interface ILogHhtbeService extends IService<LogHhtbe> {

}
