package com.catcoin.consume.service.impl;

import com.catcoin.consume.model.LogHhtbe;
import com.catcoin.consume.mapper.LogHhtbeMapper;
import com.catcoin.consume.service.ILogHhtbeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * Hhtb流水 服务实现类
 * </p>
 *
 * @author gxz
 * @since 2018-09-12
 */
@Service
public class LogHhtbeServiceImpl extends ServiceImpl<LogHhtbeMapper, LogHhtbe> implements ILogHhtbeService {

}
