package com.catcoin.consume.service;

import com.catcoin.consume.model.Account;
import com.catcoin.consume.model.LogMailcc;
import com.baomidou.mybatisplus.service.IService;
import com.catcoin.consume.result.Result;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户消费账户流水表 服务类
 * </p>
 *
 * @author consume
 * @since 2018-09-03
 */
public interface ILogMailccService extends IService<LogMailcc> {


    Result record(Integer pageNum,LogMailcc logMailcc);

    void add(LogMailcc logMailcc);
    List<Map<String,Object>> getrecord(Integer userId);
}
