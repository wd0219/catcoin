package com.catcoin.consume.service;

import com.catcoin.consume.model.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author caody
 * @since 2018-08-31
 */
public interface IUserService extends IService<User> {

}
