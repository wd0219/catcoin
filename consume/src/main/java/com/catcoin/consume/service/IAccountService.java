package com.catcoin.consume.service;

import com.catcoin.consume.model.Account;
import com.baomidou.mybatisplus.service.IService;
import com.catcoin.consume.model.LogMailcc;
import com.catcoin.consume.result.Result;

import java.util.List;

/**
 * <p>
 * 用户账户表 服务类
 * </p>
 *
 * @author consume
 * @since 2018-09-03
 */
public interface IAccountService extends IService<Account> {

    Result balance(Integer pageNum, Account account);
    Account selectByUserId(Integer userId);

    List<Account> getAccount(Integer userId);
}
