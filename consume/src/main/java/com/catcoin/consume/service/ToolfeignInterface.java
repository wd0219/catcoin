package com.catcoin.consume.service;

import com.catcoin.consume.service.impl.FeignFallbackService;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value="tool",fallback = FeignFallbackService.class)
public interface ToolfeignInterface {

    @RequestMapping("/test1")
    public String test1();

    @RequestMapping("/operate")
    public Object operate(@RequestParam(value = "userID") String userID,@RequestParam(value = "coinID") String coinID,
                          @RequestParam(value = "isBuy") String isBuy,@RequestParam(value = "amount") String amount);


    @RequestMapping("/ticker")
    public String ticker();

    @RequestMapping("/getBalance")
    public String getBalance(@RequestParam(value = "userID") String userID, @RequestParam(value = "coinID") String coinID);
}
