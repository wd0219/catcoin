package com.catcoin.catcoinzuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class CatCoinZuulApplication {
    public static void main(String[] args) {
        SpringApplication.run(CatCoinZuulApplication.class, args);
    }

}
