package com.catcoin.deposit.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

@SuppressWarnings("all")
public class MSGUtil {
    /** 记录日志的变量 */
    private static Logger logger = LoggerFactory.getLogger(MSGUtil.class);

    private static final int[] weight = new int[] { 7, 9, 10, 5, 8, 4, 2, 1, 6,
            3, 7, 9, 10, 5, 8, 4, 2, 1 }; // 加权因子
    private static final int[] checkDigit = new int[] { 1, 0, 'X', 9, 8, 7, 6,
            5, 4, 3, 2, };

    public MSGUtil() {
    }

    /**
     * 计算18位身份证的校验码
     */
    private static String getCheckDigit(String eighteenCardId) {
        int remaining = 0;
        //先取得18位身份证的17位本体码
        if(eighteenCardId.length() == 18) {
            eighteenCardId = eighteenCardId.substring(0, 17);
        }
        //根据公式对本体码进行加权求和
        if(eighteenCardId.length() == 17) {
            int sum = 0;
            int[] a = new int[17];
            //先对前17位数字的权求和
            for(int i = 0; i < 17; i++) {
                String k = eighteenCardId.substring(i, i+1);
                a[i] = Integer.parseInt(k);
            }
            for(int i = 0; i < 17; i++) {
                sum = sum + weight[i] * a[i];
            }
            //再与11取模
            remaining = sum % 11;
        }
        //返回校验码
        return remaining == 2 ? "X" : String.valueOf(checkDigit[remaining]);
    }

    /**
     * 将15位身份证号码升级为18位身份证号码
     */
    private static String update2eighteen(String fifteenCardID) {
        //15位身份证上的生日的年份没有加19，要加上
        String eighteenCardID = fifteenCardID.substring(0,6);
        eighteenCardID = eighteenCardID + "19";
        eighteenCardID = eighteenCardID + fifteenCardID.substring(6,15);
        eighteenCardID = eighteenCardID + getCheckDigit(eighteenCardID);
        return eighteenCardID;
    }

    /**
     * 验证身份证是否符合格式
     */
    public static boolean verify(String idcard) {
        idcard=idcard.toUpperCase();
        if(idcard.length() == 15) {
            idcard = update2eighteen(idcard);
        }
        if(idcard.length() != 18) {
            return false;
        }
        //获取身份证上的最后一位，它是校验码
        String checkDigit = idcard.substring(17,18);
        //比较获取的校验码与生成的校验码是否相等
        if (checkDigit.equals(getCheckDigit(idcard))) {
            return true;
        }
        return false;
    }


    /**
     * 是否为空判断(trim)
     *
     * @param str
     * @return
     */
    public static boolean isBlank(String str) {
        return str == null || "".equals(str.trim());
    }

    /**
     * 是否为空判断(trim)
     *
     * @param obj
     * @return
     */
    public static boolean isBlank(Object obj){
        if (obj == null)
            return true;
        if (obj instanceof CharSequence)
            return ((CharSequence) obj).toString().trim().equals("");
        if (obj instanceof Collection)
            return ((Collection) obj).isEmpty();
        if (obj instanceof Map)
            return ((Map) obj).isEmpty();
        if (obj instanceof Object[]) {
            Object[] object = (Object[]) obj;
            if (object.length == 0) {
                return true;
            }
            boolean empty = true;
            for (int i = 0; i < object.length; i++) {
                if (!isEmpty(object[i])) {
                    empty = false;
                    break;
                }
            }
            return empty;
        }
        return false;
    }

    /**
     * 是否为空判断(无trim)
     *
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }


    /**
     * 是否为空判断
     *
     * @param obj
     * @return
     */
    public static boolean isEmpty(Object obj){
        if (obj == null)
            return true;
        if (obj instanceof CharSequence)
            return ((CharSequence) obj).length() == 0;
        if (obj instanceof Collection)
            return ((Collection) obj).isEmpty();
        if (obj instanceof Map)
            return ((Map) obj).isEmpty();
        if (obj instanceof Object[]) {
            Object[] object = (Object[]) obj;
            if (object.length == 0) {
                return true;
            }
            boolean empty = true;
            for (int i = 0; i < object.length; i++) {
                if (!isEmpty(object[i])) {
                    empty = false;
                    break;
                }
            }
            return empty;
        }
        return false;
    }

    /**
     * 验证码6位
     */
    public static String getCode() {
        String code = new Random().nextInt(1000000) + "";
        if (code.length() < 6) {
            return getCode();
        }
        return code;
    }

    /**
     * 校验手机号
     * @param phone
     * @return
     */
    public static boolean getResponsePhone(String phone){
        if(phone.startsWith("1")&&phone.length()==11) {
            return true;
        }
        else {
            return false;
        }
    }

     /*
    校验过程：
    1、从卡号最后一位数字开始，逆向将奇数位(1、3、5等等)相加。
    2、从卡号最后一位数字开始，逆向将偶数位数字，先乘以2（如果乘积为两位数，将个位十位数字相加，即将其减去9），再求和。
    3、将奇数位总和加上偶数位总和，结果应该可以被10整除。
    */
    /**
     * 校验银行卡卡号
     */
    public static boolean checkBankCard(String bankCard) {
        if(bankCard.length() < 15 || bankCard.length() > 19) {
            return false;
        }
        char bit = getBankCardCheckCode(bankCard.substring(0, bankCard.length() - 1));
        if(bit == 'N'){
            return false;
        }
        return bankCard.charAt(bankCard.length() - 1) == bit;
    }

    /**
     * 从不含校验位的银行卡卡号采用 Luhm 校验算法获得校验位
     * @param nonCheckCodeBankCard
     * @return
     */
    public static char getBankCardCheckCode(String nonCheckCodeBankCard){
        if(nonCheckCodeBankCard == null || nonCheckCodeBankCard.trim().length() == 0
                || !nonCheckCodeBankCard.matches("\\d+")) {
            //如果传的不是数据返回N
            return 'N';
        }
        char[] chs = nonCheckCodeBankCard.trim().toCharArray();
        int luhmSum = 0;
        for(int i = chs.length - 1, j = 0; i >= 0; i--, j++) {
            int k = chs[i] - '0';
            if(j % 2 == 0) {
                k *= 2;
                k = k / 10 + k % 10;
            }
            luhmSum += k;
        }
        return (luhmSum % 10 == 0) ? '0' : (char)((10 - luhmSum % 10) + '0');
    }

    // 过滤特殊字符(名称类)
    public static String StringFilter(String str) throws PatternSyntaxException {
        // 只允许字母和数字 // String regEx = "[^a-zA-Z0-9]";
        // 清除掉所有特殊字符
        String regEx = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、_？a-zA-Z0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.replaceAll("").trim();
    }


   /* public static void main(String args[]){
        String str="14232619760223052x";
        str=str.toUpperCase();
        System.out.println(str);
        boolean b = MSGUtil.verify(str);
        System.out.println(b+"=====================");
    }*/

}

