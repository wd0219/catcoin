package com.catcoin.deposit.util;

import java.util.UUID;

/**
 * token生成策略
 */
public class TokenGenerate {

    /**
     * 生成token
     * @return
     */
    public static String GetGUID()
    {
        return UUID.randomUUID().toString().replace("-", "").substring(0,30);
    }

}
