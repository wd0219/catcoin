package com.catcoin.deposit.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.catcoin.deposit.mapper.UsablecatccMapper;
import com.catcoin.deposit.model.Usablecatcc;
import com.catcoin.deposit.model.UsablecatccDto;
import com.catcoin.deposit.service.IUsablecatccService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户账户充值表 服务实现类
 * </p>
 *
 * @author slt
 * @since 2018-09-16
 */
@Service
public class UsablecatccServiceImpl extends ServiceImpl<UsablecatccMapper, Usablecatcc> implements IUsablecatccService {
    @Autowired
    private UsablecatccMapper usablecatccMapper;
    @Override
    public List<UsablecatccDto> selectAllq2List() {
        List<UsablecatccDto> list = usablecatccMapper.selectAllq2List();
        return list;
    }
}
