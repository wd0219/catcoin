package com.catcoin.deposit.service.impl;

import com.catcoin.deposit.model.LogMailcc;
import com.catcoin.deposit.mapper.LogMailccMapper;
import com.catcoin.deposit.service.ILogMailccService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户消费账户流水表 服务实现类
 * </p>
 *
 * @author consume
 * @since 2018-09-04
 */
@Service
public class LogMailccServiceImpl extends ServiceImpl<LogMailccMapper, LogMailcc> implements ILogMailccService {

}
