package com.catcoin.deposit.service;


import com.baomidou.mybatisplus.service.IService;
import com.catcoin.deposit.model.Usablecatcc;
import com.catcoin.deposit.model.UsablecatccDto;

import java.util.List;

/**
 * <p>
 * 用户账户充值表 服务类
 * </p>
 *
 * @author slt
 * @since 2018-09-16
 */
public interface IUsablecatccService extends IService<Usablecatcc> {

    List<UsablecatccDto> selectAllq2List();
}
