package com.catcoin.deposit.service;

import com.catcoin.deposit.model.Inviteid;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author gxz
 * @since 2018-09-05
 */
public interface IInviteidService extends IService<Inviteid> {

}
