package com.catcoin.deposit.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.catcoin.deposit.mapper.AccountMapper;
import com.catcoin.deposit.model.Account;
import com.catcoin.deposit.model.AccountDto;
import com.catcoin.deposit.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户账户表 服务实现类
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {
    @Autowired
    private AccountMapper accountMapper;
    @Override
    public List<AccountDto> selectAllq2List() {
        List<AccountDto> list = accountMapper.selectAllq2List();
        return list;
    }
}
