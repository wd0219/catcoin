package com.catcoin.deposit.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.catcoin.deposit.mapper.UsersBankcardsMapper;
import com.catcoin.deposit.model.UsersBankcards;
import com.catcoin.deposit.service.IUsersBankcardsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户的银行卡表 服务实现类
 * </p>
 *
 * @author 1111
 * @since 2018-09-10
 */
@Service
public class UsersBankcardsServiceImpl extends ServiceImpl<UsersBankcardsMapper, UsersBankcards> implements IUsersBankcardsService {
    @Autowired
    private UsersBankcardsMapper usersBankcardsMapper;
    @Override
    public List<Map<String,Object>> selectBankList(Integer userId) {
        List<Map<String,Object>> list = usersBankcardsMapper.selectBankList(userId);
        return list;
    }
}
