package com.catcoin.deposit.service;

import com.baomidou.mybatisplus.service.IService;
import com.catcoin.deposit.model.Account;
import com.catcoin.deposit.model.AccountDto;

import java.util.List;

/**
 * <p>
 * 用户账户表 服务类
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
public interface IAccountService extends IService<Account> {

    List<AccountDto> selectAllq2List();
}
