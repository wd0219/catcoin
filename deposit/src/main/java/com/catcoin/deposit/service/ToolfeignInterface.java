package com.catcoin.deposit.service;

import com.catcoin.deposit.service.impl.FeignFallbackService;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="tool",fallback = FeignFallbackService.class)
public interface ToolfeignInterface {

    @RequestMapping("/test2")
    public String test2();

    @RequestMapping("/register")
    public String register(@RequestParam(value = "userMobile") String userMobile);


    @RequestMapping("/md5")
    public String getMd5Result(@RequestParam(value = "values")String values);

    //获取交易所用户id
    @RequestMapping("/register")
    public String getHHTBUserId(@RequestParam(value = "userMobile") String userMobile);
    /**
     * 获取用户余额
     * @param userID 交易所用户id
     * @param coinID 币种  21=HHTB,17=MAOC猫币
     * @return
     */
    @RequestMapping("/getBalance")
    public String getBalance(@RequestParam(value = "userID") String userID, @RequestParam(value = "coinID") String coinID);

    @RequestMapping("/ticker")
    public String ticker();


    @RequestMapping("/operate")
    public String operate(@RequestParam(value = "userID")String userID,@RequestParam(value = "coinID")String coinID,
                          @RequestParam(value = "isBuy")String isBuy,@RequestParam(value = "amount")String amount);

}
