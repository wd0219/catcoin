package com.catcoin.deposit.service.impl;

import com.catcoin.deposit.service.ToolfeignInterface;
import org.springframework.stereotype.Service;


@Service
public class FeignFallbackService implements ToolfeignInterface {
    @Override
    public String test2() {
        return "error";
    }

    @Override
    public String register(String userMobile) {
        return "error";
    }

    @Override
    public String getMd5Result(String values ) {
        return "error";
    }

    @Override
    public String getHHTBUserId(String userMobile) {
        return "error";
    }

    @Override
    public String getBalance(String userID, String coinID) {
        return "error";
    }

    @Override
    public String ticker() {
        return "error";
    }


    @Override
    public String operate(String userID, String coinID, String isBuy, String amount) {
        return "error";
    }
}
