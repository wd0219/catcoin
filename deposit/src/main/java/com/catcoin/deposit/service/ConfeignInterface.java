package com.catcoin.deposit.service;

import com.catcoin.deposit.service.impl.ConFeignFallbackService;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name="consume",fallback = ConFeignFallbackService.class)
public interface ConfeignInterface {

    @RequestMapping("/test1")
    public String test1();

}
