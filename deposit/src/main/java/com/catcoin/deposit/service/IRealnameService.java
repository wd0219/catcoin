package com.catcoin.deposit.service;

import com.baomidou.mybatisplus.service.IService;
import com.catcoin.deposit.model.Realname;
import com.catcoin.deposit.result.AppResult;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 实名认证信息表 服务类
 * </p>
 *
 * @author 1111
 * @since 2018-09-10
 */
public interface IRealnameService extends IService<Realname> {
    Realname selectUsersRealNameByUserId(Integer userId);

    Realname selectUsersRealNameByCardId(String id);

    AppResult addRealname(String userPhone, String accUser, String ID, Integer userId, String accNo, HttpServletRequest request);
}
