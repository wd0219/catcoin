package com.catcoin.deposit.service;

import com.baomidou.mybatisplus.service.IService;
import com.catcoin.deposit.model.UsersBankcards;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户的银行卡表 服务类
 * </p>
 *
 * @author 1111
 * @since 2018-09-10
 */
public interface IUsersBankcardsService extends IService<UsersBankcards> {

    List<Map<String,Object>> selectBankList(Integer userId);
}
