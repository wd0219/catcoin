package com.catcoin.deposit.service.impl;



import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.catcoin.deposit.mapper.AccountMapper;
import com.catcoin.deposit.mapper.LogStatedccMapper;
import com.catcoin.deposit.model.Account;
import com.catcoin.deposit.model.LogStatedcc;
import com.catcoin.deposit.model.SysConfigs;
import com.catcoin.deposit.service.ILogStatedccService;
import com.catcoin.deposit.service.ISysConfigsService;
import com.catcoin.deposit.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 猫币定期流水表 服务实现类
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
@Service
public class LogStatedccServiceImpl extends ServiceImpl<LogStatedccMapper, LogStatedcc> implements ILogStatedccService {
    @Autowired
    private LogStatedccMapper logStatedccMapper;
    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private ISysConfigsService iConfigsService;
    @Override
    public BigDecimal getMaobiCount(Integer userId) {
        BigDecimal bigDecimal = logStatedccMapper.getMaobiCount(userId);
        return bigDecimal;
    }

    @Override
    public BigDecimal selectConfig(String fieldCode) {
        SysConfigs configs = new SysConfigs();
        configs.setFieldCode(fieldCode);
        EntityWrapper<SysConfigs> entityWrapper = new EntityWrapper<SysConfigs>(configs);
        configs = iConfigsService.selectOne(entityWrapper);
        return new BigDecimal(configs.getFieldValue());
    }
    @Override
    public List<Map<String,Object>> getMaoBiList(Integer userId) {
        List<Map<String,Object>> list = logStatedccMapper.selectListAll(userId);
        return list;
    }

    @Override
    @Transactional
    public String httbCVMaoBi(BigDecimal jbhttb, BigDecimal lvHttb,Integer userId,String ticker) {
        String result = "error";
        try{
            //查询用户账户表
            Account accountSet = new Account();
            accountSet.setUserId(userId);
            accountSet.setStatus(1);
            Account account = accountMapper.selectOne(accountSet);
            if(StringUtils.isBlank(account)){
                return "用户不存在";
            }
            //转换成RMB
            BigDecimal statedccc = jbhttb.multiply(lvHttb.setScale(4,BigDecimal.ROUND_HALF_UP));
            //转换猫币
            BigDecimal statedcc = statedccc.multiply(new BigDecimal(ticker)).setScale(4,BigDecimal.ROUND_HALF_UP);
            //添加定期记录
            LogStatedcc logStatedcc = new LogStatedcc();
            logStatedcc.setCreateTime(new Date());
            logStatedcc.setDataFlag(1);
            logStatedcc.setFromId(userId);
            logStatedcc.setUserId(userId);
            logStatedcc.setOrderNo(StringUtils.getOrderIdByTime("6"));
            logStatedcc.setType(1);
            logStatedcc.setRemark("HTTB复投");
            logStatedcc.setPreStatedcc(account.getStatedcc());
            logStatedcc.setStatedcc(statedcc);
            logStatedcc.setStatedccType(1);

            Integer iLog = logStatedccMapper.insert(logStatedcc);
            //修改用户账户表
            account.setHhtbe(jbhttb);
            account.setStatedcc(account.getStatedcc().add(statedcc));
            Integer iAccount = accountMapper.updateById(account);

            if(iLog.intValue() != 0 && iAccount.intValue() != 0){
                result = "ok";
            }
        }catch (Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }

        return result;
    }


}
