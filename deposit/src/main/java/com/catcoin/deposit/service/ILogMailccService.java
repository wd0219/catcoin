package com.catcoin.deposit.service;

import com.catcoin.deposit.model.LogMailcc;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户消费账户流水表 服务类
 * </p>
 *
 * @author consume
 * @since 2018-09-04
 */
public interface ILogMailccService extends IService<LogMailcc> {

}
