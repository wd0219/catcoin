package com.catcoin.deposit.service;

import com.catcoin.deposit.model.User;
import com.baomidou.mybatisplus.service.IService;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author caody
 * @since 2018-08-31
 */
public interface IUserService extends IService<User> {
    public Map<String,Object> login(String userphone, String password);
    public Map<String,Object> findShare(String checkData);
    public Map<String,Object> getBalnce(String userId);
    public Map<String,Object> regist(String loginName, String num,  String userPhone,  String checkData,  String loginPwd);
    public Map<String,Object> loginBycode(@PathVariable String userPhone, String num);

    //阿里云图片上传
    public Map<String,Object> upload(HttpServletRequest req);

    //获取推荐人信息
    public User recommendInfo(String userId);

    //获取我推荐的人的列表
    public List<User> recommendList(Integer userId);

    //获取我推荐的人的列表(分页)
    public List<Map<String,Object>> recommendListNew(Integer userId);

    public Integer recommendListCount(Integer userId);
}
