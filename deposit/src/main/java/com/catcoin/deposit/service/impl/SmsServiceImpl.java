package com.catcoin.deposit.service.impl;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.catcoin.deposit.aliyun.ALiYunOss;
import com.catcoin.deposit.service.ISmsService;
import com.catcoin.deposit.util.CacheUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class SmsServiceImpl implements ISmsService {

    @Autowired
    private ALiYunOss aLiYunOss;
    @Autowired
    CacheUtil cacheUtil;

    /**
     * 用户注册验证接口
     */
    @Override
    public Object send_register_message(String number) {


        IClientProfile iClientProfile = DefaultProfile.getProfile("cn-hangzhou","LTAI0oVvPRNYwt4p","NoUX3bIHDKrdYRBuEOSkx4DkNSq9Fe");
        try {
            DefaultProfile.addEndpoint("cn-hangzhou","cn-hangzhou",aLiYunOss.getProduct(),aLiYunOss.getDomain());
        } catch (ClientException e) {
            e.printStackTrace();
        }
        IAcsClient acsClient = new DefaultAcsClient(iClientProfile);
        //随机生成六位验证码
        int code = (int)((Math.random()*9+1)*100000);
        //删除该号码上次的验证码记录

        //保存到数据库

        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
        request.setPhoneNumbers(number);

        //必填:短信签名-可在短信控制台中找到，你在签名管理里的内容
        //request.setSignName("中澜网络");
        request.setSignName("华猫商城");
        //request.setSignName("盛开金融");

        //必填:短信模板-可在短信控制台中找到，你模板管理里的模板编号
        //request.setTemplateCode("SMS_136388932");

        //request.setTemplateCode("SMS_120120131");
        //request.setTemplateCode("SMS_119077955");
        //request.setTemplateCode("SMS_119092823");
        //request.setTemplateCode("SMS_119083003");
        request.setTemplateCode("SMS_119077952");
        //request.setTemplateCode("SMS_69305110");

        //request.setTemplateCode("SMS_69305110");

        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        request.setTemplateParam("{\"code\":\""+code+"\"}");

        //hint 此处可能会抛出异常，注意catch
        SendSmsResponse sendSmsResponse = null;
        try {
            sendSmsResponse = acsClient.getAcsResponse(request);
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }

        /**
         * 先判断缓存中有没有手机号对应的值，
         * 如果有更新值
         * 如果没有添加值
         */
        if(cacheUtil.isKeyInCache("mb:"+number)){
            cacheUtil.updateAndTime("mb:"+number,code,(long)(60*5));
        }else{
            cacheUtil.set("mb:"+number,code,(long)(60*5));
        };

        //获取发送状态
        String cod = sendSmsResponse.getCode();
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("code",cod);
        map.put("num",code);
        return map;
    }
}
