package com.catcoin.deposit.service.impl;

import com.catcoin.deposit.model.SysConfigs;
import com.catcoin.deposit.mapper.SysConfigsMapper;
import com.catcoin.deposit.service.ISysConfigsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统配置表 服务实现类
 * </p>
 *
 * @author consume
 * @since 2018-09-04
 */
@Service
public class SysConfigsServiceImpl extends ServiceImpl<SysConfigsMapper, SysConfigs> implements ISysConfigsService {

}
