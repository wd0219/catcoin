package com.catcoin.deposit.service;

import com.baomidou.mybatisplus.service.IService;
import com.catcoin.deposit.model.LogStatedcc;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 猫币定期流水表 服务类
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
public interface ILogStatedccService extends IService<LogStatedcc> {

    BigDecimal getMaobiCount(Integer userId);

    List<Map<String,Object>> getMaoBiList(Integer userId);

    String httbCVMaoBi(BigDecimal jbhttb, BigDecimal lvHttb,Integer userId,String ticker);

    BigDecimal selectConfig(String fieldCode);
}
