package com.catcoin.deposit.service.impl;

import com.catcoin.deposit.model.Inviteid;
import com.catcoin.deposit.mapper.InviteidMapper;
import com.catcoin.deposit.service.IInviteidService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author gxz
 * @since 2018-09-05
 */
@Service
public class InviteidServiceImpl extends ServiceImpl<InviteidMapper, Inviteid> implements IInviteidService {

}
