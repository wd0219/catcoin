package com.catcoin.deposit.service;

/**
 * 阿里云短信发送接口
 */
public interface ISmsService {

    /**
     * 用户注册验证接口
     */
    public Object send_register_message(String phone);

}
