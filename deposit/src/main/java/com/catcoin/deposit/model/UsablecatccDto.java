package com.catcoin.deposit.model;

import java.math.BigDecimal;

/**
 * <p>
 * 用户账户充值表
 * </p>
 *
 * @author slt
 * @since 2018-09-16
 */
public class UsablecatccDto extends Usablecatcc {
    private BigDecimal statedcc2;   //千分之2
    private BigDecimal statedcc;    //猫币定期
    private BigDecimal blancecc;    //猫币余额
    private BigDecimal mailcc;      //商城可消费余额
    private Integer accountId;      //用户账户id
    private String userPhone;       //用户电话

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public BigDecimal getStatedcc2() {
        return statedcc2;
    }

    public void setStatedcc2(BigDecimal statedcc2) {
        this.statedcc2 = statedcc2;
    }

    public BigDecimal getStatedcc() {
        return statedcc;
    }

    public void setStatedcc(BigDecimal statedcc) {
        this.statedcc = statedcc;
    }

    public BigDecimal getBlancecc() {
        return blancecc;
    }

    public void setBlancecc(BigDecimal blancecc) {
        this.blancecc = blancecc;
    }

    public BigDecimal getMailcc() {
        return mailcc;
    }

    public void setMailcc(BigDecimal mailcc) {
        this.mailcc = mailcc;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }
}
