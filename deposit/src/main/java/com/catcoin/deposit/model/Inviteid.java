package com.catcoin.deposit.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author gxz
 * @since 2018-09-05
 */
public class Inviteid extends Model<Inviteid> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @TableField("userId")
    private Integer userId;
    /**
     * 用户手机号
     */
    @TableField("userPhone")
    private String userPhone;
    /**
     * 推荐人id
     */
    @TableField("inviteId")
    private Integer inviteId;
    /**
     * 推荐人手机号
     */
    @TableField("invitePhone")
    private String invitePhone;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Integer getInviteId() {
        return inviteId;
    }

    public void setInviteId(Integer inviteId) {
        this.inviteId = inviteId;
    }

    public String getInvitePhone() {
        return invitePhone;
    }

    public void setInvitePhone(String invitePhone) {
        this.invitePhone = invitePhone;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Inviteid{" +
        "id=" + id +
        ", userId=" + userId +
        ", userPhone=" + userPhone +
        ", inviteId=" + inviteId +
        ", invitePhone=" + invitePhone +
        "}";
    }
}
