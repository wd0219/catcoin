package com.catcoin.deposit.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author wudi
 * @since 2018-09-03
 */
@TableName("hhtbUsers")
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "userId", type = IdType.AUTO)

    private Integer userId;
    /**
     * 手机
     */
    @TableField("userPhone")
    private String userPhone;
    /**
     * 登录名 昵称
     */
    @TableField("userName")
    private String userName;
    /**
     * 密码
     */
    @TableField("password")
    private String password;
    /**
     * 安全码
     */
    @TableField("salt")
    private Integer salt;
    /**
     * 支付密码
     */
    @TableField("payPwd")
    private String payPwd;
    /**
     * 用户类型 2000太阳线2000用户 5000太阳线5000用户 10000太阳线10000用户
     */
    @TableField("userType")
    private Integer userType;
    /**
     * 会员头像
     */
    @TableField("userPhoto")
    private String userPhoto;
    /**
     * 账号状态：0停用 1启用
     */
    @TableField("userStatus")
    private Integer userStatus;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;
    /**
     * 是否允许转换积分
     */
    @TableField("isOperation")
    private Integer isOperation;
    /**
     * 真实姓名
     */
    @TableField("trueName")
    private String trueName;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSalt() {
        return salt;
    }

    public void setSalt(Integer salt) {
        this.salt = salt;
    }

    public String getPayPwd() {
        return payPwd;
    }

    public void setPayPwd(String payPwd) {
        this.payPwd = payPwd;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getIsOperation() {
        return isOperation;
    }

    public void setIsOperation(Integer isOperation) {
        this.isOperation = isOperation;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    @Override
    protected Serializable pkVal() {
        return this.userId;
    }

    @Override
    public String toString() {
        return "User{" +
        "userId=" + userId +
        ", userPhone=" + userPhone +
        ", userName=" + userName +
        ", password=" + password +
        ", salt=" + salt +
        ", payPwd=" + payPwd +
        ", userType=" + userType +
        ", userPhoto=" + userPhoto +
        ", userStatus=" + userStatus +
        ", createTime=" + createTime +
        ", isOperation=" + isOperation +
        ", trueName=" + trueName +
        "}";
    }
}
