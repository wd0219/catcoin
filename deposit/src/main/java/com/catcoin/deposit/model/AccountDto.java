package com.catcoin.deposit.model;

import java.math.BigDecimal;

public class AccountDto extends Account {
    private BigDecimal statedcc2;//千分之2
    private String userPhone;
    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }
    public BigDecimal getStatedcc2() {
        return statedcc2;
    }

    public void setStatedcc2(BigDecimal statedcc2) {
        this.statedcc2 = statedcc2;
    }
}
