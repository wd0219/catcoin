package com.catcoin.deposit.model;

public class UserDto extends User {
    private String createtimes;

    private String hhtbId;

    private Boolean isppwd;

    public String getCreatetimes() {
        return createtimes;
    }

    public void setCreatetimes(String createtimes) {
        this.createtimes = createtimes;
    }

    public String getHhtbId() {
        return hhtbId;
    }

    public void setHhtbId(String hhtbId) {
        this.hhtbId = hhtbId;
    }

    public Boolean getIsppwd() {
        return isppwd;
    }

    public void setIsppwd(Boolean isppwd) {
        this.isppwd = isppwd;
    }
}
