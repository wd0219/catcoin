package com.catcoin.deposit.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户消费账户流水表
 * </p>
 *
 * @author consume
 * @since 2018-09-04
 */
@TableName("log_mailcc")
public class LogMailcc extends Model<LogMailcc> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 目标用户ID 0为平台
     */
    @TableField("userId")
    private Integer userId;
    /**
     * 		发起用户ID 0为平台
     */
    @TableField("fromId")
    private Integer fromId;
    /**
     * 对应订单号
     */
    @TableField("orderNo")
    private String orderNo;
    /**
     * 操作前的数量
     */
    @TableField("preMailcc")
    private BigDecimal preMailcc;
    /**
     * 变化的数量
     */
    @TableField("mailcc")
    private BigDecimal mailcc;
    /**
     * 流水标志 -1减少 1增加
     */
    @TableField("mailccType")
    private Integer mailccType;
    /**
     * 备注
     */
    @TableField("remark")
    private String remark;
    /**
     * 有效状态 1有效 0删除
     */
    @TableField("dataFlag")
    private Integer dataFlag;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;
    /**
     * 类型
     */
    @TableField("type")
    private Integer type;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFromId() {
        return fromId;
    }

    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getPreMailcc() {
        return preMailcc;
    }

    public void setPreMailcc(BigDecimal preMailcc) {
        this.preMailcc = preMailcc;
    }

    public BigDecimal getMailcc() {
        return mailcc;
    }

    public void setMailcc(BigDecimal mailcc) {
        this.mailcc = mailcc;
    }

    public Integer getMailccType() {
        return mailccType;
    }

    public void setMailccType(Integer mailccType) {
        this.mailccType = mailccType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "LogMailcc{" +
        "id=" + id +
        ", userId=" + userId +
        ", fromId=" + fromId +
        ", orderNo=" + orderNo +
        ", preMailcc=" + preMailcc +
        ", mailcc=" + mailcc +
        ", mailccType=" + mailccType +
        ", remark=" + remark +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        ", type=" + type +
        "}";
    }
}
