package com.catcoin.deposit.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.catcoin.deposit.model.Account;
import com.catcoin.deposit.model.AccountDto;

import java.util.List;

/**
 * <p>
 * 用户账户表 Mapper 接口
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
public interface AccountMapper extends BaseMapper<Account> {

    List<AccountDto> selectAllq2List();
}
