package com.catcoin.deposit.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.catcoin.deposit.model.Realname;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 实名认证信息表 Mapper 接口
 * </p>
 *
 * @author 1111
 * @since 2018-09-10
 */
public interface RealnameMapper extends BaseMapper<Realname> {

    Realname selectUsersRealNameByUserId(@Param(value = "userId") Integer userId);

    Realname selectUsersRealNameByCardId(@Param(value = "cardID")String cardID);
}
