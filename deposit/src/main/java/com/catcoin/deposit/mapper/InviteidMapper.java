package com.catcoin.deposit.mapper;

import com.catcoin.deposit.model.Inviteid;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author gxz
 * @since 2018-09-05
 */
public interface InviteidMapper extends BaseMapper<Inviteid> {

}
