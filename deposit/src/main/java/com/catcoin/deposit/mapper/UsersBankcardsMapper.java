package com.catcoin.deposit.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.catcoin.deposit.model.UsersBankcards;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户的银行卡表 Mapper 接口
 * </p>
 *
 * @author 1111
 * @since 2018-09-10
 */
public interface UsersBankcardsMapper extends BaseMapper<UsersBankcards> {

    List<Map<String,Object>> selectBankList(Integer userId);
}
