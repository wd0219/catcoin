package com.catcoin.deposit.mapper;

import com.catcoin.deposit.model.LogMailcc;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户消费账户流水表 Mapper 接口
 * </p>
 *
 * @author consume
 * @since 2018-09-04
 */
public interface LogMailccMapper extends BaseMapper<LogMailcc> {

}
