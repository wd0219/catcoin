package com.catcoin.deposit.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.catcoin.deposit.model.LogStatedcc;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 猫币定期流水表 Mapper 接口
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
public interface LogStatedccMapper extends BaseMapper<LogStatedcc> {

    BigDecimal getMaobiCount(@Param(value = "userId") Integer userId);

    List<Map<String,Object>> selectListAll(@Param(value = "userId")Integer userId);
}
