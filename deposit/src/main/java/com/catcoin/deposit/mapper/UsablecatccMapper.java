package com.catcoin.deposit.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.catcoin.deposit.model.Usablecatcc;
import com.catcoin.deposit.model.UsablecatccDto;

import java.util.List;

/**
 * <p>
 * 用户账户充值表 Mapper 接口
 * </p>
 *
 * @author slt
 * @since 2018-09-16
 */
public interface UsablecatccMapper extends BaseMapper<Usablecatcc> {

    List<UsablecatccDto> selectAllq2List();
}
