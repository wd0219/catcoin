package com.catcoin.deposit.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.catcoin.deposit.model.User;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author wudi
 * @since 2018-09-03
 */
public interface UserMapper extends BaseMapper<User> {
    List<Map<String,Object>> selectListAll(Integer userId);
    Map<String,Object> selectLoginMessages(Integer userId);

}
