package com.catcoin.deposit.aop;


import com.alibaba.fastjson.JSONObject;
import com.catcoin.deposit.mongodb.CatCoinLog;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

@Aspect
@Component
@Slf4j
public class LogAop {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Pointcut("execution(public * com.catcoin.deposit.controller.*.*(..))")
    public void logAop() {
    }

    @Before("logAop()")
    public void around(JoinPoint joinPoint) {
        log.info("user:cdy");
        log.info("time:" + new Date());
        log.info("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature
                ().getName());
        log.info("ARGS : " + Arrays.toString(joinPoint.getArgs()));
        String userId = null;
        //       userId = cacheUtil.get("catCoinUserId").toString();
        String logname = null;
        //       logname = cacheUtil.get("catCoinUserName").toString();
        CatCoinLog catCoinLog = new CatCoinLog();
//        catCoinLog.setLogname(logname);
        catCoinLog.setLogTypeId("1");
        catCoinLog.setLogtype("业务日志");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String s = simpleDateFormat.format(new Date());
        catCoinLog.setCreatetime(s);
//        catCoinLog.setUserid(Integer.parseInt(userId));
        catCoinLog.setClassname(joinPoint.getSignature().getDeclaringTypeName());
        catCoinLog.setMethod(joinPoint.getSignature().getName());
        catCoinLog.setSucceed("成功");
        catCoinLog.setMessage("");
        catCoinLog.setArgs(Arrays.toString(joinPoint.getArgs()));
        JSONObject jsonObject = (JSONObject) JSONObject.toJSON(catCoinLog);
        String content = jsonObject.toJSONString();
        rabbitTemplate.convertAndSend("catCoin", content);
    }

    @AfterThrowing(pointcut = "logAop()", throwing = "e")
    public void afterThrowing(JoinPoint joinPoint, Throwable e) {
        log.info("user:cdy");
        log.info("time:" + new Date());
        log.info("path : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature
                ().getName());
        log.info("param : " + Arrays.toString(joinPoint.getArgs()));
        log.info("异常代码:" + e.getClass().getName());
        log.info("异常信息:" + e.getMessage());

        String userId = null;
//        userId = cacheUtil.get("catCoinUserId").toString();
        String logname = null;
        //       logname = cacheUtil.get("catCoinUserName").toString();
        CatCoinLog catCoinLog = new CatCoinLog();
//        catCoinLog.setLogname(logname);
        catCoinLog.setLogTypeId("2");
        catCoinLog.setLogtype("异常日志");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String s = simpleDateFormat.format(new Date());
        catCoinLog.setCreatetime(s);
//        catCoinLog.setUserid(Integer.parseInt(userId));
        catCoinLog.setClassname(joinPoint.getSignature().getDeclaringTypeName());
        catCoinLog.setMethod(joinPoint.getSignature().getName());
        catCoinLog.setSucceed("失败");
        catCoinLog.setMessage("");
        catCoinLog.setArgs(Arrays.toString(joinPoint.getArgs()));
        JSONObject jsonObject = (JSONObject) JSONObject.toJSON(catCoinLog);
        String content = jsonObject.toJSONString();
        rabbitTemplate.convertAndSend("catCoin", content);

    }
}
