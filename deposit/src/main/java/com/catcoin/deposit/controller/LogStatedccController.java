package com.catcoin.deposit.controller;


import com.catcoin.deposit.model.LogStatedcc;
import com.catcoin.deposit.result.AppResult;
import com.catcoin.deposit.service.ConfeignInterface;
import com.catcoin.deposit.service.ILogStatedccService;
import com.catcoin.deposit.service.ToolfeignInterface;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 猫币定期流水表 前端控制器
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
@RestController
@CrossOrigin
public class LogStatedccController {
    @Autowired
    private ILogStatedccService iLogStatedccService;
    @Autowired
    private ConfeignInterface confeignInterface;
    @Autowired
    private ToolfeignInterface toolfeignInterface;


    /**
     * 获取猫币数量
     * @param userId 用户id
     * @return 成功ok
     */
    @RequestMapping("/getMaoBiCount")
    public AppResult getMaobiCount(Integer userId){
        BigDecimal maobiCount = iLogStatedccService.getMaobiCount(userId);
        //查询配置属性表
        //猫币定期转强制消费比例(百分比)
        String fieldCode = "statedccToForce";
        BigDecimal cvXiaoFeiRatio = iLogStatedccService.selectConfig(fieldCode);
        //猫币定期转猫币余额
        fieldCode = "statedccToBlancecc";
        BigDecimal  cvMaoBiRatio = iLogStatedccService.selectConfig(fieldCode);
        //猫币定期每日转出率(千分比）
        fieldCode = "statedccPercent";
        BigDecimal dayCVMaoBiRatio = iLogStatedccService.selectConfig(fieldCode);

        StringBuffer remark = new StringBuffer();
        remark.append("猫币定期账户中猫币来源为HHTB购买所得。");
        remark.append("猫币定期账户中的猫币每天自动释放").append(dayCVMaoBiRatio);
        remark.append("‰，其中").append(cvXiaoFeiRatio).append("％自动转入消费账户中，消费账户中猫币只能用于购物。");
        remark.append("另外").append(cvMaoBiRatio).append("%自动转入猫币余额账户，猫币余额账户中的猫币可以进行理财，购物和交易。");
        Map<String,String> map = new HashMap<>();

        map.put("maobiCount",maobiCount.toString());
        map.put("remark",remark.toString());
        return  AppResult.OK(map);
    }


    /**
     * 获取猫币定期流水
     * @param userId 用户id
     * @return 成功ok
     */
    @RequestMapping("/getMaoBiList")
    public AppResult getMaoBiList(Integer userId,Integer pageNum){
        Integer pageSize = 10;
        PageHelper.startPage(pageNum,pageSize);
        List<Map<String,Object>> list = iLogStatedccService.getMaoBiList(userId);
        PageInfo<Map<String,Object>> info = new PageInfo<>(list);
        Integer totalPages = info.getPages();
        return  AppResult.OK(totalPages,"success",list);
    }

    /**
     * 交易所hhtb转换猫币定期
     * @param jbhttb hhtb转换的
     * @param userId 用户id
     * @return 成功ok
     */
    @RequestMapping("/httbCVMaoBi")
    public AppResult httbCVMaoBi(BigDecimal jbhttb, Integer userId){
        String result = "error";
        BigDecimal lvHttb = new BigDecimal(6.52);
        String ticker = toolfeignInterface.ticker();
        if(!"error".equals(ticker)){
            result = iLogStatedccService.httbCVMaoBi(jbhttb,lvHttb,userId,ticker);
            if("ok".equals(result)){
                return AppResult.OK(result);
            }
        }

        return  AppResult.ERROR(result);
    }



}

