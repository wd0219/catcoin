package com.catcoin.deposit.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author gxz
 * @since 2018-09-05
 */
@Controller
@RequestMapping("/deposit/inviteid")
public class InviteidController {

}

