package com.catcoin.deposit.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 用户的银行卡表 前端控制器
 * </p>
 *
 * @author 1111
 * @since 2018-09-10
 */
@Controller
@RequestMapping("/hhtb/usersBankcards")
public class UsersBankcardsController {

}

