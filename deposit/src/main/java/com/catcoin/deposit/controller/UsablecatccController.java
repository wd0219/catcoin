package com.catcoin.deposit.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 用户账户充值表 前端控制器
 * </p>
 *
 * @author slt
 * @since 2018-09-16
 */
@Controller
@RequestMapping("/usablecatcc/usablecatcc")
public class UsablecatccController {

}

