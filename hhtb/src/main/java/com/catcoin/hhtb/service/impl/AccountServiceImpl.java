package com.catcoin.hhtb.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.catcoin.hhtb.model.Account;
import com.catcoin.hhtb.mapper.AccountMapper;
import com.catcoin.hhtb.model.SysConfigs;
import com.catcoin.hhtb.service.*;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.catcoin.hhtb.util.StringUtils;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * <p>
 * 用户账户表 服务实现类
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
@Service
@Log
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {

    @Autowired
    private IAccountService accountService;

    @Autowired
    private ISysConfigsService sysConfigsService;

    @Autowired
    private ILogHhtbcService logHhtbcService;

    @Autowired
    private ILogStatedccService logStatedccService;

    @Autowired
    private ToolfeignInterface toolfeignInterface;

    @Autowired
    private IUsablecatccService usablecatccService;

    @Transactional
    public  Integer hhtbToStatedcc(Integer userId,String hhtbNum){
        BigDecimal hhtb = new BigDecimal(hhtbNum);
        Account account = new Account();
        account.setUserId(userId);
        EntityWrapper<Account> entityWrapperNew = new EntityWrapper<Account>(account);
        List<Account> listAccount = accountService.selectList(entityWrapperNew);
        account = listAccount.get(0);
        BigDecimal zeroNew = new BigDecimal(0);
        if(-1==account.getHhtbc().compareTo(hhtb)){
            return 2;
        }else if(0==hhtb.compareTo(zeroNew)){
            return 3;
        }else{
            BigDecimal hhtbcNow=account.getHhtbc().subtract(hhtb);
            account.setHhtbc(hhtbcNow);
            //hhtbc流水（hhtbc转化猫币定期），余额减少
            String orderNo = StringUtils.getOrderIdByTime("6");
            logHhtbcService.insertLogHhtbc(userId, orderNo,account.getHhtbc(),hhtb,-1, "hhtb提币到猫币定期", 1);
            EntityWrapper entityWrapper = new EntityWrapper();
            SysConfigs sysConfigs = new SysConfigs();
            sysConfigs.setFieldCode("hhtbRate");
            entityWrapper.setEntity(sysConfigs);
            List<SysConfigs> listSysConfigs =sysConfigsService.selectList(entityWrapper);
            BigDecimal hhtbRate = new BigDecimal(listSysConfigs.get(0).getFieldValue());
            String ticker = toolfeignInterface.ticker();
            BigDecimal tickerNew = new BigDecimal(ticker);
            BigDecimal statedccUpdate = hhtb.multiply(hhtbRate).multiply(tickerNew);
            BigDecimal statedccNew = account.getStatedcc().add(statedccUpdate);
            /**
             * 截取小数后6位
             */
            BigDecimal result = statedccNew.setScale(6, RoundingMode.DOWN);
            account.setStatedcc(result);
            //猫币定期流水（hhtbc转化猫币定期流水），余额减少
            logStatedccService.insertLogStatedcc(userId,orderNo,account.getStatedcc(),statedccUpdate,1,"hhtb提币到猫币定期",0);
            usablecatccService.insertUserablecatcc(userId,orderNo,statedccUpdate);
            //最后更新账户的hhtb与猫币定期
            accountService.updateById(account);

            return 1;
        }

    }


}
