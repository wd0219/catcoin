package com.catcoin.hhtb.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.catcoin.hhtb.mapper.LogHhtbcMapper;
import com.catcoin.hhtb.model.Account;
import com.catcoin.hhtb.model.LogHhtbc;
import com.catcoin.hhtb.service.ILogHhtbcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 现金转换的HHTB流水表 服务实现类
 * </p>
 *
 * @author wangang
 * @since 2018-09-15
 */
@Service
public class LogHhtbcServiceImpl extends ServiceImpl<LogHhtbcMapper, LogHhtbc> implements ILogHhtbcService {
    @Autowired
    private AccountServiceImpl accountService;

    public void insertLogHhtbc(Integer userId,  String orderNo, BigDecimal preHHTBC, BigDecimal hhtbc, Integer hhtbcType, String remark, Integer type) {
        LogHhtbc logHhtbc = new LogHhtbc();
        logHhtbc.setUserId(userId);
        logHhtbc.setFromId(userId);
        logHhtbc.setOrderNo(orderNo);
        logHhtbc.setPreHHTBC(preHHTBC);
        logHhtbc.setHhtbc(hhtbc);
        logHhtbc.setHHTBCType(hhtbcType);
        logHhtbc.setRemark(remark);
        logHhtbc.setDataFlag(1);
        logHhtbc.setCreateTime(new Date());
        logHhtbc.setType(type);
        this.insert(logHhtbc);
    }
}
