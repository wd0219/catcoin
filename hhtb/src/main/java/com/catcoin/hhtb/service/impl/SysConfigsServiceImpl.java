package com.catcoin.hhtb.service.impl;

import com.catcoin.hhtb.model.SysConfigs;
import com.catcoin.hhtb.mapper.SysConfigsMapper;
import com.catcoin.hhtb.service.ISysConfigsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统配置表 服务实现类
 * </p>
 *
 * @author cdy
 * @since 2018-09-07
 */
@Service
public class SysConfigsServiceImpl extends ServiceImpl<SysConfigsMapper, SysConfigs> implements ISysConfigsService {

}
