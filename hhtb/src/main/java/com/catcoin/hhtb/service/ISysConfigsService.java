package com.catcoin.hhtb.service;

import com.catcoin.hhtb.model.SysConfigs;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 系统配置表 服务类
 * </p>
 *
 * @author cdy
 * @since 2018-09-07
 */
public interface ISysConfigsService extends IService<SysConfigs> {

}
