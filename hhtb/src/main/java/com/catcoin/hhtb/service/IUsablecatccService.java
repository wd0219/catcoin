package com.catcoin.hhtb.service;

import com.catcoin.hhtb.model.Usablecatcc;
import com.baomidou.mybatisplus.service.IService;

import java.math.BigDecimal;

/**
 * <p>
 * 用户账户充值表 服务类
 * </p>
 *
 * @author wangang
 * @since 2018-09-16
 */
public interface IUsablecatccService extends IService<Usablecatcc> {
    public void insertUserablecatcc(Integer userId,String orderNo, BigDecimal usablecatccNum);

}
