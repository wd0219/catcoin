package com.catcoin.hhtb.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

@FeignClient(name="consume")
public interface ConfeignInterface {

    @RequestMapping("/test1")
    public String test1();

    @RequestMapping("/logMailcc/into")
    public String into(@RequestParam("userId") String userId,@RequestParam("mailcc") BigDecimal mailcc);

    @RequestMapping("/logMailcc/record")
    public String record(@RequestParam("pageIndex") String  pageIndex);
}
