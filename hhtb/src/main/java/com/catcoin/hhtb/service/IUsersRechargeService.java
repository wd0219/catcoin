package com.catcoin.hhtb.service;

import com.baomidou.mybatisplus.service.IService;
import com.catcoin.hhtb.model.UsersRecharge;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 * 用户充值表 服务类
 * </p>
 *
 * @author 
 * @since 2018-09-15
 */
public interface IUsersRechargeService extends IService<UsersRecharge> {
    public String aliyunUpload(HttpServletRequest request);
    public Map<String,Object> addUsersRecharge(HttpServletRequest request);
    public Map<String,Object> addImg(HttpServletRequest request);
}
