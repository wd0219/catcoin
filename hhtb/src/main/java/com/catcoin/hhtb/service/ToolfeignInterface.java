package com.catcoin.hhtb.service;

import com.catcoin.hhtb.service.impl.FeignFallbackService;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="tool",fallback = FeignFallbackService.class)
public interface ToolfeignInterface {

    @RequestMapping("/test2")
    public String test2();

    @RequestMapping("/operate")
    public String operate(@RequestParam(value = "userID") String userID,@RequestParam(value = "coinID") String coinID,
                          @RequestParam(value = "isBuy") String isBuy,@RequestParam(value = "amount") String amount);

    @RequestMapping("/getBalance")
    public String getBalance(@RequestParam(value = "userID") String userID, @RequestParam(value = "coinID") String coinID);

    @RequestMapping("/register")
    public String register(@RequestParam(value = "userMobile") String userMobile);

    @RequestMapping("/ticker")
    public String ticker();

    @RequestMapping("/getFunds")
    public String getFunds(@RequestParam(value = "userID") String userID, @RequestParam(value = "coinID") String coinID,
                           @RequestParam(value = "page") String page);
}
