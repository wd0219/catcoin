package com.catcoin.hhtb.service.impl;

import com.catcoin.hhtb.service.ConfeignInterface;

import java.math.BigDecimal;

public class ConFeignFallbackService implements ConfeignInterface {
    @Override
    public String test1() {
        return "1111111111qaa";
    }

    @Override
    public String into(String userId, BigDecimal mailcc) {
        return "error";
    }

    @Override
    public String record(String pageIndex) {
        return "error";
    }
}
