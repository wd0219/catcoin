package com.catcoin.hhtb.service.impl;

import com.catcoin.hhtb.model.Hhtbusers;
import com.catcoin.hhtb.mapper.HhtbusersMapper;
import com.catcoin.hhtb.service.IHhtbusersService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author caody
 * @since 2018-09-05
 */
@Service
public class HhtbusersServiceImpl extends ServiceImpl<HhtbusersMapper, Hhtbusers> implements IHhtbusersService {

}
