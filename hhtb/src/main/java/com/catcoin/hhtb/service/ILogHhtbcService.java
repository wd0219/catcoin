package com.catcoin.hhtb.service;

import com.baomidou.mybatisplus.service.IService;
import com.catcoin.hhtb.model.LogHhtbc;

import java.math.BigDecimal;

/**
 * <p>
 * 现金转换的HHTB流水表 服务类
 * </p>
 *
 * @author wangang
 * @since 2018-09-15
 */
public interface ILogHhtbcService extends IService<LogHhtbc> {
    public void insertLogHhtbc(Integer userId, String orderNo, BigDecimal preHHTBC, BigDecimal hhtbc, Integer hhtbcType, String remark, Integer type);

}
