package com.catcoin.hhtb.service;

import com.catcoin.hhtb.model.Account;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户账户表 服务类
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
public interface IAccountService extends IService<Account> {
    public  Integer hhtbToStatedcc(Integer userId,String hhtbNum);

}
