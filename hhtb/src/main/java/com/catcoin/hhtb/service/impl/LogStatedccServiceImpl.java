package com.catcoin.hhtb.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.catcoin.hhtb.mapper.LogStatedccMapper;
import com.catcoin.hhtb.model.LogStatedcc;
import com.catcoin.hhtb.service.ILogStatedccService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 猫币定期流水表 服务实现类
 * </p>
 *
 * @author wangang
 * @since 2018-09-15
 */
@Service
public class LogStatedccServiceImpl extends ServiceImpl<LogStatedccMapper, LogStatedcc> implements ILogStatedccService {

    public void insertLogStatedcc(Integer userId, String orderNo, BigDecimal preStatedcc,BigDecimal statedcc,Integer statedccType,String remark,Integer type){
        LogStatedcc logStatedcc = new LogStatedcc();
        logStatedcc.setUserId(userId);
        logStatedcc.setFromId(userId);
        logStatedcc.setOrderNo(orderNo);
        logStatedcc.setPreStatedcc(preStatedcc);
        logStatedcc.setStatedcc(statedcc);
        logStatedcc.setStatedccType(statedccType);
        logStatedcc.setRemark(remark);
        logStatedcc.setDataFlag(1);
        logStatedcc.setCreateTime(new Date());
        logStatedcc.setType(type);
        this.insert(logStatedcc);
    }

}
