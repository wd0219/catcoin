package com.catcoin.hhtb.service.impl;

import com.catcoin.hhtb.service.DepfeignInterface;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

@Service
public class DepFeignFallbackService implements DepfeignInterface {
    @Override
    public String httbCVMaoBi(BigDecimal jbhttb, Integer userId) {
        return "error";
    }
    @Override
    public String aliyunUpload(HttpServletRequest request) {
        return "ERROR";
    }

}
