package com.catcoin.hhtb.service;

import com.baomidou.mybatisplus.service.IService;
import com.catcoin.hhtb.model.LogStatedcc;

import java.math.BigDecimal;

/**
 * <p>
 * 猫币定期流水表 服务类
 * </p>
 *
 * @author wangang
 * @since 2018-09-15
 */
public interface ILogStatedccService extends IService<LogStatedcc> {

    public void insertLogStatedcc(Integer userId, String orderNo, BigDecimal preStatedcc, BigDecimal statedcc, Integer statedccType, String remark, Integer type);

}
