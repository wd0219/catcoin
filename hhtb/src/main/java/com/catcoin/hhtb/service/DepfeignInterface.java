package com.catcoin.hhtb.service;

import com.catcoin.hhtb.service.impl.DepFeignFallbackService;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

@FeignClient(name="deposit",fallback = DepFeignFallbackService.class)
public interface DepfeignInterface {

    @RequestMapping("/httbCVMaoBi")
    public String httbCVMaoBi(@RequestParam(value = "jbhttb") BigDecimal jbhttb, @RequestParam(value = "userId") Integer userId);
    @RequestMapping("/aliyunUpload")
    public String aliyunUpload(HttpServletRequest request);
}
