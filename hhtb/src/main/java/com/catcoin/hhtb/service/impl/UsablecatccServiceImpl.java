package com.catcoin.hhtb.service.impl;

import com.catcoin.hhtb.model.Usablecatcc;
import com.catcoin.hhtb.mapper.UsablecatccMapper;
import com.catcoin.hhtb.service.IUsablecatccService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 用户账户充值表 服务实现类
 * </p>
 *
 * @author wangang
 * @since 2018-09-16
 */
@Service
public class UsablecatccServiceImpl extends ServiceImpl<UsablecatccMapper, Usablecatcc> implements IUsablecatccService {
    public void insertUserablecatcc(Integer userId,String orderNo, BigDecimal usablecatccNum){
        Usablecatcc usablecatcc = new Usablecatcc();
        usablecatcc.setUserId(userId);
        usablecatcc.setOrderNo(orderNo);
        usablecatcc.setUsablecatcc(usablecatccNum);
        usablecatcc.setRemainderCatcc(usablecatccNum);
        usablecatcc.setCreattime(new Date());
        usablecatcc.setChangetime(new Date());
        this.insert(usablecatcc);
    }

}
