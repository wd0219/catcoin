package com.catcoin.hhtb.service.impl;

import com.catcoin.hhtb.service.ToolfeignInterface;
import org.springframework.stereotype.Service;

@Service
public class FeignFallbackService implements ToolfeignInterface {
    @Override
    public String test2() {
        return "ERROR";
    }

    @Override
    public String operate(String userID, String coinID, String isBuy, String amount) {
        return "error";
    }

    @Override
    public String getBalance(String userID, String coinID) {
        return "error";
    }

    @Override
    public String register(String userMobile) {
        return "error";
    }

    @Override
    public String ticker() {
        return "error";
    }

    @Override
    public String getFunds(String userID, String coinID,String page) {
        return "error";
    }

}
