package com.catcoin.hhtb.service;

import com.catcoin.hhtb.model.Hhtbusers;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author caody
 * @since 2018-09-05
 */
public interface IHhtbusersService extends IService<Hhtbusers> {

}
