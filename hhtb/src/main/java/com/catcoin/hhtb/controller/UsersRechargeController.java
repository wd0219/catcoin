package com.catcoin.hhtb.controller;


import com.catcoin.hhtb.result.AppResult;
import com.catcoin.hhtb.service.IUsersRechargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 * 用户充值表 前端控制器
 * </p>
 *
 * @author 
 * @since 2018-09-15
 */
@CrossOrigin
@RestController
public class UsersRechargeController {
    @Autowired
    private IUsersRechargeService usersRechargeService;

    @RequestMapping("/addUsersRecharge")
    public AppResult addUsersRecharge(HttpServletRequest request){

        Map<String, Object> map = usersRechargeService.addUsersRecharge(request);
        if ("00".equals(map.get("code").toString())){
          return   AppResult.ERROR(map.get("msg").toString());
        }

        return  AppResult.OK(map.get("orderNo"));
    }
    @RequestMapping("/addImg")
    public AppResult addImg(HttpServletRequest request){
        Map<String, Object> map = usersRechargeService.addImg(request);
        if ("01".equals(map.get("code").toString())){
          return   AppResult.OK(map);
        }else {
          return   AppResult.ERROR(map.get("msg").toString());
        }
    }
}

