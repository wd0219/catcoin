package com.catcoin.hhtb.controller;

import com.catcoin.hhtb.service.ConfeignInterface;
import com.catcoin.hhtb.service.ToolfeignInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class TestController {
    @Autowired
    private ToolfeignInterface toolfeignInterface;

    @Autowired
    private ConfeignInterface confeignInterface;


    @Value("${server.port}")
    String port;

    @Value("${spring.application.name}")
    String serviceName;

    @RequestMapping("/")
    public String index(){
        return "serviceName=" + serviceName + "-------port=" + port;
    }

    @RequestMapping("/test1")
    public Object  test1(){
        return confeignInterface.test1();

    }


    @RequestMapping("/test2")
    public Object test(){
        return  toolfeignInterface.test2();
    }

    @RequestMapping("/ticker")
    public Object ticker(){
        return toolfeignInterface.ticker();
    }

    @RequestMapping("/info1")
    public Object into(){
        BigDecimal bigDecimal = new BigDecimal("11");
        return confeignInterface.into("305312",bigDecimal);
    }

    @RequestMapping("/record")
    public Object record(){
        return confeignInterface.record("1");
    }

}
