package com.catcoin.hhtb.controller;

import com.catcoin.hhtb.model.Hhtbusers;
import com.catcoin.hhtb.result.AppResult;
import com.catcoin.hhtb.service.IHhtbusersService;
import com.catcoin.hhtb.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin
public class CheckPayPwdController {

    @Autowired
    private IHhtbusersService hhtbusersService;

    /**
     *
     * 校验支付密码是否正确
     *
     * @param userId
     * @param payPwd
     * @return
     */
    @RequestMapping("/checkPayPwd")
    public AppResult checkPayPwd(String userId , String payPwd){
        Map<String,Object> map = new HashMap<String,Object>();
        Hhtbusers hhtbusers = hhtbusersService.selectById(userId);
        if(hhtbusers.getPayPwd() == null){
            return AppResult.ERROR("请您先去设置支付密码");
        }
        String encrypt = MD5Util.encrypt(payPwd + hhtbusers.getSalt());
        if(!encrypt.equals(hhtbusers.getPayPwd())){
            return AppResult.ERROR("支付密码错误，请重新输入");
        }
        return AppResult.OK("密码正确");
    }

}
