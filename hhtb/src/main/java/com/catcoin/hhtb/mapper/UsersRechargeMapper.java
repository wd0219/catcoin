package com.catcoin.hhtb.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.catcoin.hhtb.model.UsersRecharge;

/**
 * <p>
 * 用户充值表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2018-09-15
 */
public interface UsersRechargeMapper extends BaseMapper<UsersRecharge> {

}
