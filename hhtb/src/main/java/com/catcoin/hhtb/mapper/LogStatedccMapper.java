package com.catcoin.hhtb.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.catcoin.hhtb.model.LogStatedcc;

/**
 * <p>
 * 猫币定期流水表 Mapper 接口
 * </p>
 *
 * @author wangang
 * @since 2018-09-15
 */
public interface LogStatedccMapper extends BaseMapper<LogStatedcc> {

}
