package com.catcoin.hhtb.mapper;

import com.catcoin.hhtb.model.Hhtbusers;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author caody
 * @since 2018-09-05
 */
public interface HhtbusersMapper extends BaseMapper<Hhtbusers> {

}
