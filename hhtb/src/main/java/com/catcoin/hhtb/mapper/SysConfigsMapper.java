package com.catcoin.hhtb.mapper;

import com.catcoin.hhtb.model.SysConfigs;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 系统配置表 Mapper 接口
 * </p>
 *
 * @author cdy
 * @since 2018-09-07
 */
public interface SysConfigsMapper extends BaseMapper<SysConfigs> {

}
