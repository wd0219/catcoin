package com.catcoin.hhtb.mapper;

import com.catcoin.hhtb.model.Usablecatcc;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户账户充值表 Mapper 接口
 * </p>
 *
 * @author wangang
 * @since 2018-09-16
 */
public interface UsablecatccMapper extends BaseMapper<Usablecatcc> {

}
