package com.catcoin.hhtb.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.catcoin.hhtb.model.LogHhtbc;

/**
 * <p>
 * 现金转换的HHTB流水表 Mapper 接口
 * </p>
 *
 * @author wangang
 * @since 2018-09-15
 */
public interface LogHhtbcMapper extends BaseMapper<LogHhtbc> {

}
