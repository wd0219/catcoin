package com.catcoin.hhtb.mapper;

import com.catcoin.hhtb.model.Account;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户账户表 Mapper 接口
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
public interface AccountMapper extends BaseMapper<Account> {

}
