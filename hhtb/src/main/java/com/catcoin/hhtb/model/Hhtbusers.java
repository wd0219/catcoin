package com.catcoin.hhtb.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author caody
 * @since 2018-09-05
 */
public class Hhtbusers extends Model<Hhtbusers> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "userId", type = IdType.AUTO)
    private Integer userId;
    /**
     * 手机
     */
    @TableField(value = "userPhone")
    private String userPhone;
    /**
     * 登录名 昵称
     */
    @TableField(value = "userName")
    private String userName;
    /**
     * 密码
     */
    @TableField(value = "password")
    private String password;
    /**
     * 安全码
     */
    @TableField(value = "salt")
    private Integer salt;
    /**
     * 支付密码
     */
    @TableField(value = "payPwd")
    private String payPwd;
    /**
     * 用户类型 0默认 2000普通 5000主管、10000经理
     */
    @TableField(value = "userType")
    private Integer userType;
    /**
     * 会员头像
     */
    @TableField(value = "userPhoto")
    private String userPhoto;
    /**
     * 账号状态：0停用 1启用
     */
    @TableField(value = "userStatus")
    private Integer userStatus;
    /**
     * 创建时间
     */
    @TableField(value = "createTime")
    private Date createTime;
    /**
     * 是否允许转换0、不允许1、允许
     */
    @TableField(value = "isOperation")
    private Integer isOperation;
    /**
     * 真实姓名
     */
    @TableField(value = "trueName")
    private String trueName;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSalt() {
        return salt;
    }

    public void setSalt(Integer salt) {
        this.salt = salt;
    }

    public String getPayPwd() {
        return payPwd;
    }

    public void setPayPwd(String payPwd) {
        this.payPwd = payPwd;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getIsOperation() {
        return isOperation;
    }

    public void setIsOperation(Integer isOperation) {
        this.isOperation = isOperation;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    @Override
    protected Serializable pkVal() {
        return this.userId;
    }

    @Override
    public String toString() {
        return "Hhtbusers{" +
        "userId=" + userId +
        ", userPhone=" + userPhone +
        ", userName=" + userName +
        ", password=" + password +
        ", salt=" + salt +
        ", payPwd=" + payPwd +
        ", userType=" + userType +
        ", userPhoto=" + userPhoto +
        ", userStatus=" + userStatus +
        ", createTime=" + createTime +
        ", isOperation=" + isOperation +
        ", trueName=" + trueName +
        "}";
    }
}
