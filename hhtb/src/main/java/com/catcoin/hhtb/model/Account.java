package com.catcoin.hhtb.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.Serializable;

/**
 * <p>
 * 用户账户表
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
public class Account extends Model<Account> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @TableField("userId")
    private Integer userId;
    /**
     * 现金余额
     */
    private BigDecimal cash;
    /**
     * 现金转换的HHTB
     */
    @TableField("HHTBC")
    private BigDecimal hhtbc;
    /**
     * 交易所复投的HHTB
     */
    @TableField("HHTBE")
    private BigDecimal hhtbe;
    /**
     * 猫币定期
     */
    private BigDecimal statedcc;
    /**
     * 猫币余额
     */
    private BigDecimal blancecc;
    /**
     * 商城可消费余额
     */
    private BigDecimal mailcc;
    /**
     * 理财余额
     */
    private BigDecimal moneycc;
    /**
     * 账户状态 0锁定 1开启
     */
    private Integer status;
    /**
     * 理财利息
     */
    private BigDecimal ieterestcc;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public BigDecimal getHhtbc() {
        return hhtbc;
    }

    public void setHhtbc(BigDecimal hhtbc) {
        this.hhtbc = hhtbc;
    }

    public BigDecimal getHhtbe() {
        return hhtbe;
    }

    public void setHhtbe(BigDecimal hhtbe) {
        this.hhtbe = hhtbe;
    }

    public BigDecimal getStatedcc() {
        return statedcc;
    }

    public void setStatedcc(BigDecimal statedcc) {
        this.statedcc = statedcc;
    }

    public BigDecimal getBlancecc() {
        return blancecc;
    }

    public void setBlancecc(BigDecimal blancecc) {
        this.blancecc = blancecc;
    }

    public BigDecimal getMailcc() {
        return mailcc;
    }

    public void setMailcc(BigDecimal mailcc) {
        this.mailcc = mailcc;
    }

    public BigDecimal getMoneycc() {
        return moneycc;
    }

    public void setMoneycc(BigDecimal moneycc) {
        this.moneycc = moneycc;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigDecimal getIeterestcc() {
        return ieterestcc;
    }

    public void setIeterestcc(BigDecimal ieterestcc) {
        this.ieterestcc = ieterestcc;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Account{" +
        "id=" + id +
        ", userId=" + userId +
        ", cash=" + cash +
        ", hhtbc=" + hhtbc +
        ", hhtbe=" + hhtbe +
        ", statedcc=" + statedcc +
        ", blancecc=" + blancecc +
        ", mailcc=" + mailcc +
        ", moneycc=" + moneycc +
        ", status=" + status +
        ", ieterestcc=" + ieterestcc +
        "}";
    }
}
