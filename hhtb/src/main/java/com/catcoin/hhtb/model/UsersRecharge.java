package com.catcoin.hhtb.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户充值表
 * </p>
 *
 * @author 
 * @since 2018-09-15
 */
@TableName("users_recharge")
public class  UsersRecharge extends Model<UsersRecharge> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 订单编号
     */
    @TableField(value = "orderNo")
    private String orderNo;
    /**
     * 用户ID
     */
    @TableField(value = "userId")
    private Integer userId;
    /**
     * 支付方式 1:线下现金支付 4第三方支付
     */
    @TableField(value = "payType")
    private Integer payType;
    /**
     * 充值总金额 
     */
    @TableField(value = "cash")
    private BigDecimal cash;
    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;
    /**
     * 状态 -1线下支付审核拒绝 0已下单 1已支付或审核通过 2已上传图片
     */
    @TableField(value = "status")
    private Integer status;
    /**
     * 有效状态 1有效 0删除
     */
    @TableField(value = "dataFlag")
    private Integer dataFlag;
    /**
     * 创建时间
     */
    @TableField(value = "createTime")
    private Date createTime;
    /**
     * 汇款单的图片
     */
    @TableField(value = "orderImg")
    private String orderImg;
    /**
     * 拒绝理由
     */
    @TableField(value = "checkRemark")
    private String checkRemark;
    /**
     * 审核员工ID
     */
    @TableField(value = "checkStaffId")
    private Integer checkStaffId;
    /**
     * 审核时间
     */
    @TableField(value = "checkTime")
    private Date checkTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOrderImg() {
        return orderImg;
    }

    public void setOrderImg(String orderImg) {
        this.orderImg = orderImg;
    }

    public String getCheckRemark() {
        return checkRemark;
    }

    public void setCheckRemark(String checkRemark) {
        this.checkRemark = checkRemark;
    }

    public Integer getCheckStaffId() {
        return checkStaffId;
    }

    public void setCheckStaffId(Integer checkStaffId) {
        this.checkStaffId = checkStaffId;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "UsersRecharge{" +
        "id=" + id +
        ", orderNo=" + orderNo +
        ", userId=" + userId +
        ", payType=" + payType +
        ", cash=" + cash +
        ", remark=" + remark +
        ", status=" + status +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        ", orderImg=" + orderImg +
        ", checkRemark=" + checkRemark +
        ", checkStaffId=" + checkStaffId +
        ", checkTime=" + checkTime +
        "}";
    }
}
