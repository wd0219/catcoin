package com.catcoin.hhtb.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 用户账户充值表
 * </p>
 *
 * @author wangang
 * @since 2018-09-16
 */
public class Usablecatcc extends Model<Usablecatcc> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @TableField("userId")
    private Integer userId;
    /**
     * 订单号
     */
    @TableField("orderNo")
    private String orderNo;
    /**
     * 用于转换的猫币基数
     */
    @TableField("usablecatcc")
    private BigDecimal usablecatcc;
    /**
     * 剩余可转换的猫币
     */
    @TableField("remainderCatcc")
    private BigDecimal remainderCatcc;
    /**
     * 创建时间
     */
    private Date creattime;
    /**
     * 最后一次修改时间
     */
    private Date changetime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public BigDecimal getUsablecatcc() {
        return usablecatcc;
    }

    public void setUsablecatcc(BigDecimal usablecatcc) {
        this.usablecatcc = usablecatcc;
    }

    public BigDecimal getRemainderCatcc() {
        return remainderCatcc;
    }

    public void setRemainderCatcc(BigDecimal remainderCatcc) {
        this.remainderCatcc = remainderCatcc;
    }

    public Date getCreattime() {
        return creattime;
    }

    public void setCreattime(Date creattime) {
        this.creattime = creattime;
    }

    public Date getChangetime() {
        return changetime;
    }

    public void setChangetime(Date changetime) {
        this.changetime = changetime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Usablecatcc{" +
        "id=" + id +
        ", userId=" + userId +
        ", orderNo=" + orderNo +
        ", usablecatcc=" + usablecatcc +
        ", remainderCatcc=" + remainderCatcc +
        ", creattime=" + creattime +
        ", changetime=" + changetime +
        "}";
    }
}
