package com.catcoin.hhtb.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 现金转换的HHTB流水表
 * </p>
 *
 * @author wangang
 * @since 2018-09-15
 */
@TableName("log_hhtbc")
public class LogHhtbc extends Model<LogHhtbc> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 目标用户ID 0为平台
     */
    @TableField("userId")
    private Integer userId;
    /**
     * 发起用户ID 0为平台
     */
    @TableField("fromId")
    private Integer fromId;
    /**
     * 对应订单号
     */
    @TableField("orderNo")
    private String orderNo;
    /**
     * 操作前的数量
     */
    @TableField("preHHTBC")
    private BigDecimal preHHTBC;
    /**
     * 变化的数量
     */
    @TableField("HHTBC")
    private BigDecimal hhtbc;
    /**
     * 流水标志 -1减少 1增加
     */
    @TableField("HHTBCType")
    private Integer HHTBCType;
    /**
     * 备注
     */
    private String remark;
    /**
     * 有效状态 1有效 0删除
     */
    @TableField("dataFlag")
    private Integer dataFlag;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;
    /**
     * 类型 0充值 1提现
     */
    @TableField("type")
    private Integer type;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFromId() {
        return fromId;
    }

    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getPreHHTBC() {
        return preHHTBC;
    }

    public void setPreHHTBC(BigDecimal preHHTBC) {
        this.preHHTBC = preHHTBC;
    }

    public BigDecimal getHhtbc() {
        return hhtbc;
    }

    public void setHhtbc(BigDecimal hhtbc) {
        this.hhtbc = hhtbc;
    }

    public Integer getHHTBCType() {
        return HHTBCType;
    }

    public void setHHTBCType(Integer HHTBCType) {
        this.HHTBCType = HHTBCType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "LogHhtbc{" +
        "id=" + id +
        ", userId=" + userId +
        ", fromId=" + fromId +
        ", orderNo=" + orderNo +
        ", preHHTBC=" + preHHTBC +
        ", hhtbc=" + hhtbc +
        ", HHTBCType=" + HHTBCType +
        ", remark=" + remark +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        ", type=" + type +
        "}";
    }
}
