package com.catcoin.hhtb.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * Hhtb流水
 * </p>
 *
 * @author cdy
 * @since 2018-09-13
 */
public class LogHhtbe extends Model<LogHhtbe> {


    private Integer id;
    /**
     * 目标用户ID 0为平台
     */
    private Integer userId;
    /**
     * 		发起用户ID 0为平台
     */
    private Integer fromId;
    /**
     * 对应订单号
     */
    private String orderNo;
    /**
     * 操作前的数量
     */
    private BigDecimal preHHTBE;
    /**
     * 变化的数量
     */
    private BigDecimal hhtbe;
    /**
     * 流水标志 -1减少 1增加
     */
    private Integer HHTBEType;
    /**
     * 备注
     */
    private String remark;
    /**
     * 有效状态 1有效 0删除
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 类型 0充值 1提现
     */
    private Integer type;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFromId() {
        return fromId;
    }

    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getPreHHTBE() {
        return preHHTBE;
    }

    public void setPreHHTBE(BigDecimal preHHTBE) {
        this.preHHTBE = preHHTBE;
    }

    public BigDecimal getHhtbe() {
        return hhtbe;
    }

    public void setHhtbe(BigDecimal hhtbe) {
        this.hhtbe = hhtbe;
    }

    public Integer getHHTBEType() {
        return HHTBEType;
    }

    public void setHHTBEType(Integer HHTBEType) {
        this.HHTBEType = HHTBEType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "LogHhtbe{" +
        "id=" + id +
        ", userId=" + userId +
        ", fromId=" + fromId +
        ", orderNo=" + orderNo +
        ", preHHTBE=" + preHHTBE +
        ", hhtbe=" + hhtbe +
        ", HHTBEType=" + HHTBEType +
        ", remark=" + remark +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        ", type=" + type +
        "}";
    }
}
