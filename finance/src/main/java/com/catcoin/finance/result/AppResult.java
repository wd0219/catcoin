package com.catcoin.finance.result;

public class AppResult {
    private String msg;
    private Integer status;
    private Object data;
    public static AppResult newAppResult(String msg, Integer status, Object data){
        return new AppResult(msg, status, data);
    }
    public AppResult(String msg, Integer status, Object data) {
        this.msg = msg;
        this.status = status;
        this.data = data;
    }
    public static AppResult OK(Object object){
        return  new AppResult("success",1,object);
    }
    public static AppResult OK(String message,Object object){
        return  new AppResult(message,1,object);
    }
    public static AppResult ERROR(String message){
        return  new AppResult(message,-1,null);
    }
    public static AppResult ERROR(String message,Integer status){
        return  new AppResult(message,status,null);
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
