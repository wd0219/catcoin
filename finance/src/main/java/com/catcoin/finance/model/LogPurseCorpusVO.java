package com.catcoin.finance.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2018/9/4 0004.
 */
public class LogPurseCorpusVO {

    private String purseCorpusCount;
    private String time;

    public String getPurseCorpusCount() {
        return purseCorpusCount;
    }

    public void setPurseCorpusCount(String purseCorpusCount) {
        this.purseCorpusCount = purseCorpusCount;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
