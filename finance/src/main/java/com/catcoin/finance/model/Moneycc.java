package com.catcoin.finance.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 理财钱包表
 * </p>
 *
 * @author baggar
 * @since 2018-09-12
 */
@TableName("moneycc")
public class Moneycc extends Model<Moneycc> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @TableField("userId")
    private Integer userId;
    /**
     * 此笔理财本金金额
     */
    @TableField("moneyccValue")
    private BigDecimal moneyccValue;
    /**
     * 此笔理财本金产生利息总和
     */
    @TableField("totalAccrual")
    private BigDecimal totalAccrual;
    /**
     * 此笔理财本金当前剩余利息
     */
    @TableField("nowAccrual")
    private BigDecimal nowAccrual;
    /**
     * 状态 0锁定(不产生利息，剩余利息可提现)   1开启(产生利息)
     */
    @TableField("status")
    private Integer status;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public BigDecimal getMoneyccValue() {
        return moneyccValue;
    }

    public void setMoneyccValue(BigDecimal moneyccValue) {
        this.moneyccValue = moneyccValue;
    }

    public BigDecimal getTotalAccrual() {
        return totalAccrual;
    }

    public void setTotalAccrual(BigDecimal totalAccrual) {
        this.totalAccrual = totalAccrual;
    }

    public BigDecimal getNowAccrual() {
        return nowAccrual;
    }

    public void setNowAccrual(BigDecimal nowAccrual) {
        this.nowAccrual = nowAccrual;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Moneycc{" +
        "id=" + id +
        ", userId=" + userId +
        ", moneyccValue=" + moneyccValue +
        ", totalAccrual=" + totalAccrual +
        ", nowAccrual=" + nowAccrual +
        ", status=" + status +
        ", createTime=" + createTime +
        "}";
    }
}
