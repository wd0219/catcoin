package com.catcoin.finance.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 猫币余额流水
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
@TableName("log_blancecc")
public class LogBlancecc extends Model<LogBlancecc> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 目标用户ID 0为平台
     */
    @TableField("userId")
    private Integer userId;
    /**
     * 		发起用户ID 0为平台
     */
    @TableField("fromId")
    private Integer fromId;
    @TableField("moneyccId")
    private Integer moneyccId;
    /**
     * 对应订单号
     */
    @TableField("orderNo")
    private String orderNo;
    /**
     * 操作前的数量
     */
    @TableField("preBlancecc")
    private BigDecimal preBlancecc;
    /**
     * 变化的数量
     */
    @TableField("blancecc")
    private BigDecimal blancecc;
    /**
     * 流水标志 -1减少 1增加
     */
    @TableField("blanceccType")
    private Integer blanceccType;
    /**
     * 备注
     */
    @TableField("remark")
    private String remark;
    /**
     * 有效状态 1有效 0删除
     */
    @TableField("dataFlag")
    private Integer dataFlag;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;
    /**
     * 类型
     */
    @TableField("type")
    private Integer type;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFromId() {
        return fromId;
    }

    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getPreBlancecc() {
        return preBlancecc;
    }

    public void setPreBlancecc(BigDecimal preBlancecc) {
        this.preBlancecc = preBlancecc;
    }

    public BigDecimal getBlancecc() {
        return blancecc;
    }

    public void setBlancecc(BigDecimal blancecc) {
        this.blancecc = blancecc;
    }

    public Integer getBlanceccType() {
        return blanceccType;
    }

    public void setBlanceccType(Integer blanceccType) {
        this.blanceccType = blanceccType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getMoneyccId() {
        return moneyccId;
    }

    public void setMoneyccId(Integer moneyccId) {
        this.moneyccId = moneyccId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "LogBlancecc{" +
                "id=" + id +
                ", userId=" + userId +
                ", fromId=" + fromId +
                ", moneyccId=" + moneyccId +
                ", orderNo='" + orderNo + '\'' +
                ", preBlancecc=" + preBlancecc +
                ", blancecc=" + blancecc +
                ", blanceccType=" + blanceccType +
                ", remark='" + remark + '\'' +
                ", dataFlag=" + dataFlag +
                ", createTime=" + createTime +
                ", type=" + type +
                '}';
    }

}
