package com.catcoin.finance.model;

/**
 * Created by Administrator on 2018/9/6 0006.
 */
public class AccountDTO extends Account {
    private String userPhone;

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    @Override
    public String toString() {
        return "AccountDTO{" +
                "userPhone='" + userPhone + '\'' +
                '}';
    }
}
