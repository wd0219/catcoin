package com.catcoin.finance.model;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 理财钱包本金流水
 * </p>
 *
 * @author baggar
 * @since 2018-09-03
 */
@TableName("log_purse_corpus")
public class LogPurseCorpus extends Model<LogPurseCorpus> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 目标用户ID 0为平台
     */
    @TableField("userId")
    private Integer userId;
    /**
     * 发起用户ID 0为平台
     */
    @TableField("fromId")
    private Integer fromId;
    /**
     * 理财钱包id
     */
    @TableField("moneyccId")
    private Integer moneyccId;
    /**
     * 对应订单号
     */
    @TableField("orderNo")
    private String orderNo;
    /**
     * 操作前的数量
     */
    @TableField("prePurseCorpus")
    private BigDecimal prePurseCorpus;
    /**
     * 变化的数量
     */
    @TableField("purseCorpus")
    private BigDecimal purseCorpus;
    /**
     * 流水标志 -1减少 1增加
     */
    @TableField("purseCorpusType")
    private Integer purseCorpusType;
    /**
     * 备注
     */
    @TableField("remark")
    private String remark;
    /**
     * 有效状态 1有效 0删除
     */
    @TableField("dataFlag")
    private Integer dataFlag;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;
    /**
     * 类型
     */
    @TableField("type")
    private Integer type;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFromId() {
        return fromId;
    }

    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getPrePurseCorpus() {
        return prePurseCorpus;
    }

    public void setPrePurseCorpus(BigDecimal prePurseCorpus) {
        this.prePurseCorpus = prePurseCorpus;
    }

    public BigDecimal getPurseCorpus() {
        return purseCorpus;
    }

    public void setPurseCorpus(BigDecimal purseCorpus) {
        this.purseCorpus = purseCorpus;
    }

    public Integer getPurseCorpusType() {
        return purseCorpusType;
    }

    public void setPurseCorpusType(Integer purseCorpusType) {
        this.purseCorpusType = purseCorpusType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getMoneyccId() {
        return moneyccId;
    }

    public void setMoneyccId(Integer moneyccId) {
        this.moneyccId = moneyccId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "LogPurseCorpus{" +
                "id=" + id +
                ", userId=" + userId +
                ", fromId=" + fromId +
                ", moneyccId=" + moneyccId +
                ", orderNo='" + orderNo + '\'' +
                ", prePurseCorpus=" + prePurseCorpus +
                ", purseCorpus=" + purseCorpus +
                ", purseCorpusType=" + purseCorpusType +
                ", remark='" + remark + '\'' +
                ", dataFlag=" + dataFlag +
                ", createTime=" + createTime +
                ", type=" + type +
                '}';
    }
}
