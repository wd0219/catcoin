package com.catcoin.finance.model;

import java.math.BigDecimal;

/**
 * Created by Administrator on 2018/9/4 0004.
 */
public class LogPurseCorpusDTO extends LogPurseCorpus{
    /**
     *理财钱包本金
     */
    private BigDecimal moneycc;

    /**
     * 理财钱包利息
     */
    private BigDecimal ieterestcc;

    public BigDecimal getIeterestcc() {
        return ieterestcc;
    }

    public void setIeterestcc(BigDecimal ieterestcc) {
        this.ieterestcc = ieterestcc;
    }

    public BigDecimal getMoneycc() {
        return moneycc;
    }

    public void setMoneycc(BigDecimal moneycc) {
        this.moneycc = moneycc;
    }

    @Override
    public String toString() {
        return "LogPurseCorpusDTO{" +
                "moneycc=" + moneycc +
                ", ieterestcc=" + ieterestcc +
                '}';
    }
}
