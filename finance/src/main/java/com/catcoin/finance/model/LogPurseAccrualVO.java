package com.catcoin.finance.model;

/**
 * Created by Administrator on 2018/9/15 0015.
 */
public class LogPurseAccrualVO {
    private String purseAccrualCount;
    private String time;

    public String getPurseAccrualCount() {
        return purseAccrualCount;
    }

    public void setPurseAccrualCount(String purseAccrualCount) {
        this.purseAccrualCount = purseAccrualCount;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
