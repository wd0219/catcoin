package com.catcoin.finance.model;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 用户账户表
 * </p>
 *
 * @author baggar
 * @since 2018-09-03
 */
@TableName("account")
public class Account extends Model<Account> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @TableField("userId")
    private Integer userId;
    /**
     * 现金余额
     */
    @TableField("cash")
    private BigDecimal cash;
    /**
     * 现金转换的HHTB
     */
    @TableField("HHTBC")
    private BigDecimal hhtbc;
    /**
     * 交易所复投的HHTB
     */
    @TableField("HHTBE")
    private BigDecimal hhtbe;
    /**
     * 猫币定期
     */
    @TableField("statedcc")
    private BigDecimal statedcc;
    /**
     * 猫币余额
     */
    @TableField("blancecc")
    private BigDecimal blancecc;
    /**
     * 商城可消费余额
     */
    @TableField("mailcc")
    private BigDecimal mailcc;
    /**
     * 理财钱包当前可用本金金额（当前金额）
     */
    @TableField("moneyccTotal")
    private BigDecimal moneyccTotal;
    /**
     * 理财钱包总充值金额（总金额）
     */
    @TableField("moneycc")
    private BigDecimal moneycc;
    /**
     * 账户状态 0锁定 1开启
     */
    @TableField("status")
    private Integer status;
    /**
     * 理财钱包当前可用利息总和
     */
    @TableField("ieterestcc")
    private BigDecimal ieterestcc;
    /**
     * 本金清零总利息(可全部提币)
     */
    @TableField("moneyToZeroIt")
    private BigDecimal moneyToZeroIt;
    /**
     * 本金未清零总利息(按总可用本金百分比提币)
     */
    @TableField("moneyNoZeroIt")
    private BigDecimal moneyNoZeroIt;
    /**
     * 理财钱包产生利息总和
     */
    @TableField("ieterestccTotal")
    private BigDecimal ieterestccTotal;
    /**
     * 理财账户是否允许产生利息
     */
    @TableField("financialStutas")
    private Integer financialStutas;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public BigDecimal getHhtbc() {
        return hhtbc;
    }

    public void setHhtbc(BigDecimal hhtbc) {
        this.hhtbc = hhtbc;
    }

    public BigDecimal getHhtbe() {
        return hhtbe;
    }

    public void setHhtbe(BigDecimal hhtbe) {
        this.hhtbe = hhtbe;
    }

    public BigDecimal getStatedcc() {
        return statedcc;
    }

    public void setStatedcc(BigDecimal statedcc) {
        this.statedcc = statedcc;
    }

    public BigDecimal getBlancecc() {
        return blancecc;
    }

    public void setBlancecc(BigDecimal blancecc) {
        this.blancecc = blancecc;
    }

    public BigDecimal getMailcc() {
        return mailcc;
    }

    public void setMailcc(BigDecimal mailcc) {
        this.mailcc = mailcc;
    }

    public BigDecimal getMoneycc() {
        return moneycc;
    }

    public void setMoneycc(BigDecimal moneycc) {
        this.moneycc = moneycc;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigDecimal getIeterestcc() {
        return ieterestcc;
    }

    public void setIeterestcc(BigDecimal ieterestcc) {
        this.ieterestcc = ieterestcc;
    }

    public Integer getFinancialStutas() {
        return financialStutas;
    }

    public void setFinancialStutas(Integer financialStutas) {
        this.financialStutas = financialStutas;
    }

    public BigDecimal getMoneyccTotal() {
        return moneyccTotal;
    }

    public void setMoneyccTotal(BigDecimal moneyccTotal) {
        this.moneyccTotal = moneyccTotal;
    }

    public BigDecimal getMoneyToZeroIt() {
        return moneyToZeroIt;
    }

    public void setMoneyToZeroIt(BigDecimal moneyToZeroIt) {
        this.moneyToZeroIt = moneyToZeroIt;
    }

    public BigDecimal getMoneyNoZeroIt() {
        return moneyNoZeroIt;
    }

    public void setMoneyNoZeroIt(BigDecimal moneyNoZeroIt) {
        this.moneyNoZeroIt = moneyNoZeroIt;
    }

    public BigDecimal getIeterestccTotal() {
        return ieterestccTotal;
    }

    public void setIeterestccTotal(BigDecimal ieterestccTotal) {
        this.ieterestccTotal = ieterestccTotal;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", userId=" + userId +
                ", cash=" + cash +
                ", hhtbc=" + hhtbc +
                ", hhtbe=" + hhtbe +
                ", statedcc=" + statedcc +
                ", blancecc=" + blancecc +
                ", mailcc=" + mailcc +
                ", moneyccTotal=" + moneyccTotal +
                ", moneycc=" + moneycc +
                ", status=" + status +
                ", ieterestcc=" + ieterestcc +
                ", moneyToZeroIt=" + moneyToZeroIt +
                ", moneyNoZeroIt=" + moneyNoZeroIt +
                ", ieterestccTotal=" + ieterestccTotal +
                ", financialStutas=" + financialStutas +
                '}';
    }
}
