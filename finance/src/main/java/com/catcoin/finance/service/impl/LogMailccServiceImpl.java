package com.catcoin.finance.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.catcoin.finance.mapper.AccountMapper;
import com.catcoin.finance.mapper.LogMailccMapper;
import com.catcoin.finance.model.Account;
import com.catcoin.finance.model.LogMailcc;
import com.catcoin.finance.model.Moneycc;
import com.catcoin.finance.service.IAccountService;
import com.catcoin.finance.service.ILogMailccService;
import com.catcoin.finance.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 用户消费账户流水表 服务实现类
 * </p>
 *
 * @author baggar
 * @since 2018-09-03
 */
@Service
public class LogMailccServiceImpl extends ServiceImpl<LogMailccMapper, LogMailcc> implements ILogMailccService {
    @Autowired
    private IAccountService iAccountService;
    @Autowired
    private LogMailccMapper logMailccMapper;

    @Override
    public Boolean addMailcc(Integer userId, BigDecimal mailcc) {
        Account account = iAccountService.selectOne(new EntityWrapper<Account>().eq("userId", userId));
        if(account.getMailcc()!=null){
            account.setMailcc(new BigDecimal("0.000"));
        }else{
            account.setMailcc(account.getMailcc().add(mailcc));
        }
        boolean result = iAccountService.update(account, new EntityWrapper<Account>().eq("userId", userId));
        LogMailcc logMailcc = new LogMailcc();
        logMailcc.setUserId(userId);
        logMailcc.setFromId(userId);
        logMailcc.setOrderNo(StringUtils.getOrderIdByTime("2"));//理财钱包提币到消费2
        if(account.getMailcc()!=null){
            logMailcc.setPreMailcc(new BigDecimal("0.000"));
        }else{
            logMailcc.setPreMailcc(logMailcc.getMailcc().add(mailcc));
        }
        logMailcc.setMailcc(mailcc);
        logMailcc.setMailccType(1);
        logMailcc.setRemark("理财钱包提币到消费");
        logMailcc.setCreateTime(new Date());
        logMailcc.setType(1);//理财钱包提币到消费
        boolean insert = this.insert(logMailcc);
        if (result && insert){
            return true;
        }
       return false;
    }

    @Override
    public BigDecimal selectMailccCountByUserId(Integer userId) {
        return logMailccMapper.selectMailccCountByUserId(userId);
    }

    /**
     * 生成消费流水
     * @param userId
     * @param mailcc  变化的数量
     * @param sign   3每天提币获得
     * @param mailccType  流水标志 -1减少 1增加
     * @param setRemark 备注 理财钱包提币到消费
     * @param type 类型 3每天提币获得
     * @return
     */
    @Override
    public Boolean insertLogMailcc(Integer userId, Moneycc moneycc,BigDecimal preVal, BigDecimal mailcc, String sign, Integer mailccType, String setRemark, Integer type) {
        Account account = iAccountService.selectOne(new EntityWrapper<Account>().eq("userId", userId));//?
        LogMailcc logMailcc = new LogMailcc();
        logMailcc.setUserId(userId);
        logMailcc.setFromId(userId);
        logMailcc.setMoneyccId(moneycc.getId());
        logMailcc.setOrderNo(StringUtils.getOrderIdByTime(sign));
        logMailcc.setPreMailcc(preVal);
        logMailcc.setMailcc(mailcc);
        logMailcc.setMailccType(mailccType);
        logMailcc.setRemark(setRemark);
        logMailcc.setDataFlag(1);
        logMailcc.setCreateTime(new Date());
        logMailcc.setType(type);
        return this.insert(logMailcc);
    }
}
