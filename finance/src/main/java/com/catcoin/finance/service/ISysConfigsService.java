package com.catcoin.finance.service;

import com.catcoin.finance.model.SysConfigs;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 系统配置表 服务类
 * </p>
 *
 * @author consume
 * @since 2018-09-04
 */
public interface ISysConfigsService extends IService<SysConfigs> {

}
