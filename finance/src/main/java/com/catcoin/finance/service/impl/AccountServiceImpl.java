package com.catcoin.finance.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.catcoin.finance.mapper.LogPurseCorpusMapper;
import com.catcoin.finance.model.Account;
import com.catcoin.finance.mapper.AccountMapper;
import com.catcoin.finance.model.AccountDTO;
import com.catcoin.finance.model.LogPurseCorpus;
import com.catcoin.finance.service.IAccountService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.catcoin.finance.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 用户账户表 服务实现类
 * </p>
 *
 * @author baggar
 * @since 2018-09-03
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {
    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private LogPurseCorpusMapper logPurseCorpusMapper;
    @Override
    public BigDecimal purseCount(Integer userId) {
        Account account = this.selectOne(new EntityWrapper<Account>().eq("userId", userId));
        BigDecimal moneycc = account.getMoneycc();
        BigDecimal ieterestcc = account.getIeterestcc();
        BigDecimal result = new BigDecimal("0.000");
        if(moneycc !=null){
            if (ieterestcc != null){
                result = moneycc.add(ieterestcc).setScale(4,BigDecimal.ROUND_UP);
            }
            result = moneycc.setScale(4,BigDecimal.ROUND_UP);
        }
        return result;
    }

    @Override
    @Transactional
    public Boolean updateAccountById(Integer userId, BigDecimal shiftToSumTotal) {
        //用户添加理财本金
        Account account = this.selectOne(new EntityWrapper<Account>().eq("userId", userId));
        BigDecimal moneycc = account.getMoneycc();
        if(moneycc !=null){
            account.setMoneycc(account.getMoneycc().add(shiftToSumTotal).setScale(4,BigDecimal.ROUND_UP));
        }else{
            account.setMoneycc(shiftToSumTotal.setScale(4,BigDecimal.ROUND_UP));
        }
        boolean result = this.update(account, new EntityWrapper<Account>().eq("userId", userId));
        //添加用户本金流水
        LogPurseCorpus logPurseCorpus = new LogPurseCorpus();
        logPurseCorpus.setUserId(userId);
        logPurseCorpus.setFromId(userId);
        logPurseCorpus.setOrderNo(StringUtils.getOrderIdByTime("1"));//猫币余额转入1
        logPurseCorpus.setPrePurseCorpus(account.getMoneycc());
        logPurseCorpus.setPurseCorpus(shiftToSumTotal);
        logPurseCorpus.setPurseCorpusType(1);
        logPurseCorpus.setRemark("猫币余额转入");
        logPurseCorpus.setDataFlag(1);
        logPurseCorpus.setCreateTime(new Date());
        logPurseCorpus.setType(1);//猫币余额转入
        Integer insert = logPurseCorpusMapper.insert(logPurseCorpus);
        if(result && 1 == insert){
            return true;
        }
        return false;
    }

    @Override
    public List<AccountDTO> selectAllList() {
        return accountMapper.selectAllList();
    }

    @Override
    public AccountDTO selectAccountDtoByUserId(Integer userId) {
        return accountMapper.selectAccountDtoByUserId(userId);
    }
}
