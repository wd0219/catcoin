package com.catcoin.finance.service.impl;

import com.catcoin.finance.service.ConsumefeignInterface;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class consumeFallbackService implements ConsumefeignInterface {
    @Override
    public String mailccinto(Integer userId, BigDecimal mailcc) {
        return "getMailccintoError";
    }
}
