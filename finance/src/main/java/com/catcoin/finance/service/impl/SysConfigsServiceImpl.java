package com.catcoin.finance.service.impl;

import com.catcoin.finance.model.SysConfigs;
import com.catcoin.finance.mapper.SysConfigsMapper;
import com.catcoin.finance.service.ISysConfigsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统配置表 服务实现类
 * </p>
 *
 * @author consume
 * @since 2018-09-04
 */
@Service
public class SysConfigsServiceImpl extends ServiceImpl<SysConfigsMapper, SysConfigs> implements ISysConfigsService {

}
