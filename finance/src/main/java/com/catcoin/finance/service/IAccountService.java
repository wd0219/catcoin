package com.catcoin.finance.service;

import com.catcoin.finance.model.Account;
import com.baomidou.mybatisplus.service.IService;
import com.catcoin.finance.model.AccountDTO;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 用户账户表 服务类
 * </p>
 *
 * @author baggar
 * @since 2018-09-03
 */
public interface IAccountService extends IService<Account> {









    /**
     * 查询所有启用状态用户及手机号
     * @return
     */
    public List<AccountDTO> selectAllList();

    /**
     * 理财钱包余额
     * @param userId
     * @return 用户理财钱包余额
     */
    public BigDecimal purseCount(Integer userId);

    /**
     *修改用户本金数量
     * @param userId
     * @param shiftToSumTotal 转入金额
     */
    public Boolean updateAccountById(Integer userId,BigDecimal shiftToSumTotal);


    /**
     * 根据用户id查询用户信息及手机号
     * @param userId
     * @return
     */
    public AccountDTO selectAccountDtoByUserId(Integer userId);
}
