package com.catcoin.finance.service;

import com.catcoin.finance.model.LogBlancecc;
import com.baomidou.mybatisplus.service.IService;

import java.math.BigDecimal;

/**
 * <p>
 * 猫币余额流水 服务类
 * </p>
 *
 * @author baggar
 * @since 2018-09-03
 */
public interface ILogBlanceccService extends IService<LogBlancecc> {
    /**
     * 理财钱包提币到猫币余额并生成流水
     * @param userId
     * @param blancecc 添加金额
     * @return
     */
    public Boolean addBlancecc(Integer userId,BigDecimal blancecc,Integer moneyccId);
    /**
     * 生成消费流水
     * @param userId
     * @param moneyccId
     * @param blancecc  变化的数量
     * @param sign   3每天提币获得 1猫币余额转入理财钱包
     * @param blanceccType  流水标志 -1减少 1增加
     * @param setRemark 备注 理财钱包提币到猫币余额  猫币余额转入理财钱包
     * @param type 类型  3每天提币获得  1猫币余额转入理财钱包
     * @return
     */
    public Boolean insertLogBlancecc(Integer userId,Integer moneyccId,BigDecimal blancecc,String sign,Integer blanceccType,String setRemark,Integer type);

    public Boolean insertLogBlancecc2(Integer userId,Integer moneyccId,BigDecimal preVal,BigDecimal blancecc,String sign,Integer blanceccType,String setRemark,Integer type);

}
