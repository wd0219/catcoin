package com.catcoin.finance.service;

import com.catcoin.finance.service.impl.consumeFallbackService;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

@FeignClient(value="consume",fallback = consumeFallbackService.class)
public interface ConsumefeignInterface {
    /**
     * 提币调用接口数据变更
     *
     * @param mailcc 变化的数量
     * @param userId 用户id
     * @return
     */
    @RequestMapping("/into")
    public String mailccinto(@RequestParam(value = "userId") Integer userId, @RequestParam(value = "mailcc") BigDecimal mailcc);
}
