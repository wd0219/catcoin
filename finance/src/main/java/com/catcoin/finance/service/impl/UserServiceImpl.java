package com.catcoin.finance.service.impl;

import com.catcoin.finance.model.User;
import com.catcoin.finance.mapper.UserMapper;
import com.catcoin.finance.service.IUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author caody
 * @since 2018-08-31
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
