package com.catcoin.finance.service;


import com.baomidou.mybatisplus.service.IService;
import com.catcoin.finance.model.LogMailcc;
import com.catcoin.finance.model.Moneycc;

import java.math.BigDecimal;

/**
 * <p>
 * 用户消费账户流水表 服务类
 * </p>
 *
 * @author baggar
 * @since 2018-09-03
 */
public interface ILogMailccService extends IService<LogMailcc> {

    /**
     * 理财钱包提币到消费
     * @param userId
     * @param mailcc 添加金额
     * @return
     */
    public Boolean addMailcc(Integer userId,BigDecimal mailcc);

    /**
     * 查询用户今日提币数量
     * @param userId
     * @return
     */
    public BigDecimal selectMailccCountByUserId(Integer userId);

    /**
     * 生成消费流水
     * @param userId
     * @param mailcc  变化的数量
     * @param sign   3每天提币获得
     * @param mailccType  流水标志 -1减少 1增加
     * @param setRemark 备注 理财钱包提币到消费
     * @param type 类型 3每天提币获得
     * @return
     */
    public Boolean insertLogMailcc(Integer userId, Moneycc moneycc, BigDecimal preVal, BigDecimal mailcc, String sign, Integer mailccType, String setRemark, Integer type);
}
