package com.catcoin.finance.service;

import com.catcoin.finance.model.Account;
import com.catcoin.finance.model.LogPurseCorpus;
import com.baomidou.mybatisplus.service.IService;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 理财钱包本金流水 服务类
 * </p>
 *
 * @author baggar
 * @since 2018-09-03
 */
public interface ILogPurseCorpusService extends IService<LogPurseCorpus> {
    /**
     * 理财钱包余额
     * @param userId
     * @return 用户理财钱包余额
     */
    public Account purseCount(Integer userId);

    /**
     *修改用户本金数量
     * @param userId
     * @param shiftToSumTotal 转入金额
     */
    public Boolean updateAccountById(Integer userId,BigDecimal shiftToSumTotal,Integer moneyccId);

    /**
     * 查看每天最高提币金额
     * @param userId
     * @return
     */
    public BigDecimal highestLimit(Integer userId);

    /**
     *  交易所猫币余额转理财钱包
     *  @Param userId
     *  @Param withdrawSum 提币金额
     */
    public Map<String,String> exchangeToPurse(Integer userId, String shiftToSum);

    /**
     *  查询理财钱包本金流水
     *  @Param userId
     *  @return   Map(k1,v1,k2,v2)1.猫币余额转入理财钱包本金流水
     *  @return   Map(k1,v1,k2,v2)1.猫币余额转入理财钱包本金流水
     *              2.理财利息达到本金清空倍数本金清空流水
     */
    public Map<String,Object> queryPurseCorpusLog(Integer userId);
}
