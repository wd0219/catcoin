package com.catcoin.finance.service.impl;

import com.catcoin.finance.service.ToolfeignInterface;
import org.springframework.stereotype.Service;

@Service
public class FeignFallbackService implements ToolfeignInterface {
    @Override
    public String test() {
        return "ERROR";
    }

    @Override
    public String register(String userMobile) {
        return "getRegisterERROR";
    }

    @Override
    public String getBalance(String userID, String coinID) {
        return "getBalanceERROR";
    }

    @Override
    public String operate(String userID, String coinID, String isBuy, String amount) {
        return "getOperateERROR";
    }
}
