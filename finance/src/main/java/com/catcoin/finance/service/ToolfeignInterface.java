package com.catcoin.finance.service;

import com.catcoin.finance.service.impl.FeignFallbackService;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value="tool",fallback = FeignFallbackService.class)
public interface ToolfeignInterface {

    @RequestMapping("/test")
    public String test();

    /**
     * 获取交易所用户Id
     * @param userMobile
     * @return
     */
    @RequestMapping("/register")
    public String register(@RequestParam(value = "userMobile")String userMobile);

    /**
     * 获取用户余额
     * @param userID 交易所用户id
     * @param coinID 币种  21=HHTB,17=MAOC猫币
     * @return
     */
    @RequestMapping("/getBalance")
    public String getBalance(@RequestParam(value = "userID")String userID, @RequestParam(value = "coinID") String coinID);

    /**
     * 用户冲币扣币
     * @param userID
     * @param coinID 币种  21=HHTB,17=MAOC猫币
     * @param isBuy 1=充币，0=扣币
     * @param amount 金额
     * @return
     */
    @RequestMapping("/operate")
    public String operate(@RequestParam(value = "userID")String userID,@RequestParam(value = "coinID")String coinID,@RequestParam(value = "isBuy")String isBuy,@RequestParam(value = "amount")String amount);


}
