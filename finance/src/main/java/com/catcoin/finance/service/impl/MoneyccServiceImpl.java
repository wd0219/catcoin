package com.catcoin.finance.service.impl;

import com.catcoin.finance.model.Moneycc;
import com.catcoin.finance.mapper.MoneyccMapper;
import com.catcoin.finance.service.IMoneyccService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 理财钱包表 服务实现类
 * </p>
 *
 * @author baggar
 * @since 2018-09-12
 */
@Service
public class MoneyccServiceImpl extends ServiceImpl<MoneyccMapper, Moneycc> implements IMoneyccService {

}
