package com.catcoin.finance.service;

import com.catcoin.finance.model.LogPurseAccrual;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 * 理财钱包利息流水 服务类
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
public interface ILogPurseAccrualService extends IService<LogPurseAccrual> {
    /**
     * 提币
     * @param userId
     * @param withdrawSum 提币金额
     * @Param type 提币类型  0本金清零的总利息(可全部提币)    1提本金未清零总利息(按总可用本金百分比提币)
     * @return
     */
    public Map<String,String> withdrawPurse(Integer userId, String withdrawSum,Integer type);

    public Map<String,Object> queryPurseAccrualLog(Integer userId);
}
