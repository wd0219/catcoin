package com.catcoin.finance.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.catcoin.finance.model.Account;
import com.catcoin.finance.model.LogBlancecc;
import com.catcoin.finance.mapper.LogBlanceccMapper;
import com.catcoin.finance.service.IAccountService;
import com.catcoin.finance.service.ILogBlanceccService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.catcoin.finance.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 猫币余额流水 服务实现类
 * </p>
 *
 * @author baggar
 * @since 2018-09-03
 */
@Service
public class LogBlanceccServiceImpl extends ServiceImpl<LogBlanceccMapper, LogBlancecc> implements ILogBlanceccService {
    @Autowired
    private IAccountService iAccountService;
    @Autowired
    private AccountServiceImpl accountService;
    @Autowired
    private LogPurseCorpusServiceImpl logPurseCorpusService;
    @Override
    public Boolean addBlancecc(Integer userId, BigDecimal blancecc,Integer moneyccId) {
        Account account = accountService.selectOne(new EntityWrapper<Account>().eq("userId", userId));
        if(account.getBlancecc()!=null){
            account.setBlancecc(new BigDecimal("0.000"));
        }else{
            account.setBlancecc(account.getMailcc().add(blancecc));
        }
        boolean result = accountService.update(account, new EntityWrapper<Account>().eq("userId", userId));
        //生成理财钱包本金到猫币余额流水
        Integer insertCorpus = logPurseCorpusService.insertLogPurseCorpus(userId, blancecc, "3", moneyccId,-1, "理财钱包转出猫币余额",3);
        if (result && insertCorpus == 1){
            return true;
        }
        return false;
    }

    @Override
    public Boolean insertLogBlancecc(Integer userId, Integer moneyccId, BigDecimal blancecc, String sign, Integer blanceccType, String setRemark, Integer type) {
        Account account = accountService.selectOne(new EntityWrapper<Account>().eq("userId", userId));
        LogBlancecc logBlancecc = new LogBlancecc();
        logBlancecc.setUserId(userId);
        logBlancecc.setFromId(userId);
        logBlancecc.setMoneyccId(moneyccId);
        logBlancecc.setOrderNo(StringUtils.getOrderIdByTime(sign));
        logBlancecc.setPreBlancecc(account.getBlancecc());
        logBlancecc.setBlancecc(blancecc);
        logBlancecc.setBlanceccType(blanceccType);
        logBlancecc.setRemark(setRemark);
        logBlancecc.setDataFlag(1);
        logBlancecc.setCreateTime(new Date());
        logBlancecc.setType(type);
        return this.insert(logBlancecc);
    }

    @Override
    public Boolean insertLogBlancecc2(Integer userId,Integer moneyccId,BigDecimal preVal,BigDecimal blancecc, String sign, Integer blanceccType, String setRemark, Integer type) {
        LogBlancecc logBlancecc = new LogBlancecc();
        logBlancecc.setUserId(userId);
        logBlancecc.setFromId(userId);
        logBlancecc.setMoneyccId(moneyccId);
        logBlancecc.setOrderNo(StringUtils.getOrderIdByTime(sign));
        logBlancecc.setPreBlancecc(preVal);
        logBlancecc.setBlancecc(blancecc);
        logBlancecc.setBlanceccType(blanceccType);
        logBlancecc.setRemark(setRemark);
        logBlancecc.setDataFlag(1);
        logBlancecc.setCreateTime(new Date());
        logBlancecc.setType(type);
        return this.insert(logBlancecc);
    }

}
