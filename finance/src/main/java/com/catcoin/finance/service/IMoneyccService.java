package com.catcoin.finance.service;

import com.catcoin.finance.model.Moneycc;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 理财钱包表 服务类
 * </p>
 *
 * @author baggar
 * @since 2018-09-12
 */
public interface IMoneyccService extends IService<Moneycc> {

}
