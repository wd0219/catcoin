package com.catcoin.finance.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.catcoin.finance.mapper.SysConfigsMapper;
import com.catcoin.finance.model.Account;
import com.catcoin.finance.model.SysConfigs;
import com.catcoin.finance.result.AppResult;
import com.catcoin.finance.result.Result;
import com.catcoin.finance.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 理财钱包本金流水 前端控制器
 * </p>
 *
 * @author baggar
 * @since 2018-09-03
 */
@RestController
public class LogPurseCorpusController {
    @Autowired
    private ILogPurseCorpusService iLogPurseCorpusService;
    @Autowired
    private ILogPurseAccrualService iLogPurseAccrualService;
    @Autowired
    private ToolfeignInterface toolfeignInterface;
    @Autowired
    private ISysConfigsService iSysConfigsService;
    @Autowired
    private IAccountService iAccountService;
    /**
     *  提币到强制消费(百分比)
     */
    private static final String withdrawToForce = "withdrawToForce";
    /**
     * 提币到余额(百分比)
     */
    private static final String withdrawToBlancecc = "withdrawToBlancecc";
    /**
     * 每日最高提出额度(本金百分比)
     */
    private static final String highestLimit = "highestLimit";
    /**
     * 理财钱包余额
     * @param userId
     * @return 用户理财钱包余额
     */
    @RequestMapping("/purseCount")
    public AppResult purseCount(Integer userId){
        Account account = iLogPurseCorpusService.purseCount(userId);
        return  AppResult.OK(account);
    }

    /**
     * 查看每天最高提币金额
     * @param userId
     * @return
     */
    @RequestMapping("/highestLimit")
    public AppResult highestLimit(Integer userId){
        BigDecimal moneycc = iLogPurseCorpusService.highestLimit(userId);
        return AppResult.OK(moneycc);
    }

    /**
     * 提币
     * @Param userId
     * @Param withdrawSum 提币金额
     * @Param type 提币类型  0本金清零的总利息(可全部提币)    1提本金未清零总利息(按总可用本金百分比提币)
     */
    @RequestMapping("/withdrawPurse")
    public AppResult withdrawPurse(Integer userId,String withdrawSum,Integer type){
        Map<String, String> result = iLogPurseAccrualService.withdrawPurse(userId, withdrawSum, type);
        if (result.containsKey("SUCCESS")){
            return AppResult.OK(result.get("SUCCESS"));
        }
        return  AppResult.ERROR(result.get("ERROR"));
    }

    /**
     *  交易所猫币余额转理财钱包
     *  @Param userId
     *  @Param shiftToSum 转出金额
     */
    @RequestMapping("/exchangeToPurse")
    public AppResult exchangeToPurse(Integer userId,String shiftToSum){
        Map<String, String> result = iLogPurseCorpusService.exchangeToPurse(userId, shiftToSum);
        if (result.containsKey("SUCCESS")){
            return AppResult.OK(result.get("SUCCESS"));
        }
        return  AppResult.ERROR(result.get("ERROR"));
    }

    @RequestMapping("/queryPurseLog")
    public AppResult queryPurseLog(Integer userId){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        Map<String, Object> PurseCorpusMap = iLogPurseCorpusService.queryPurseCorpusLog(userId);
        Map<String, Object> PurseAccrualMap = iLogPurseAccrualService.queryPurseAccrualLog(userId);
        resultMap.putAll(PurseCorpusMap);
        resultMap.putAll(PurseAccrualMap);
        return AppResult.OK(resultMap);
    }
}

