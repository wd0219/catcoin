package com.catcoin.finance.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.catcoin.finance.model.SysConfigs;
import com.catcoin.finance.result.AppResult;
import com.catcoin.finance.service.ISysConfigsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 系统配置表 前端控制器
 * </p>
 *
 * @author baggar
 * @since 2018-09-10
 */
@Controller
@RequestMapping("/sysConfigs")
public class SysConfigsController {
    @Autowired
    private ISysConfigsService iSysConfigsService;
    /**
     * 查看关于我们 联系我们 更新版本
     * @param message aboutUs
     * @param message contactUs
     * @param message updateVersion
     * @return
     */
    //@RequestMapping(value = "/getAboutUs")
    @RequestMapping(value = "/getMessage")
    @ResponseBody
    public AppResult getAboutUs(String message){
        SysConfigs sysConfigs = iSysConfigsService.selectOne(new EntityWrapper<SysConfigs>().eq("fieldCode", message));
        if(sysConfigs!=null)
            return AppResult.OK("success",sysConfigs);
            //return AppResult.OK("success",sysConfigs.getFieldValue());
        return AppResult.ERROR("error");
    }


    /**
     * 查看联系我们
     * @return
     */
   /* @RequestMapping(value = "/getContactUs")
    public AppResult getContactUs(){
        return null;
    }*/

    /**
     * 查看更新版本
     * @return
     */
   /* @RequestMapping(value = "/getUpdateVersion")
    public AppResult getUpdateVersion(){
        return null;
    }*/
}

