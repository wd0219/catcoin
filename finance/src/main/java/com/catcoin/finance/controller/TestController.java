package com.catcoin.finance.controller;

import com.catcoin.finance.service.ToolfeignInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @Autowired
    private ToolfeignInterface toolfeignInterface;


    @Value("${server.port}")
    String port;

    @Value("${spring.application.name}")
    String serviceName;

    @RequestMapping("/")
    public String index(){
        return "serviceName=" + serviceName + "-------port=" + port;
    }


    @RequestMapping("/test")
    public Object test(){
        return  toolfeignInterface.test();
    }


}
