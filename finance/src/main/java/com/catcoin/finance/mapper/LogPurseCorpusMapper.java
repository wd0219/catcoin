package com.catcoin.finance.mapper;

import com.catcoin.finance.model.LogPurseCorpus;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 理财钱包本金流水 Mapper 接口
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
public interface LogPurseCorpusMapper extends BaseMapper<LogPurseCorpus> {
    List<Map<String,String>> LogPurseCorpusMapper(@Param(value = "userId") Integer userId, @Param(value = "purseCorpusType") int purseCorpusType, @Param(value = "type") int type);
}
