package com.catcoin.finance.mapper;

import com.catcoin.finance.model.LogBlancecc;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 * 猫币余额流水 Mapper 接口
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
public interface LogBlanceccMapper extends BaseMapper<LogBlancecc> {
    /**查询用户当日提币数量
     * @param userId
     * @return
     */
    BigDecimal selectBlanceccCountByUserId(@Param(value ="userId")Integer userId);

}
