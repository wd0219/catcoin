package com.catcoin.finance.mapper;

import com.catcoin.finance.model.Moneycc;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 理财钱包表 Mapper 接口
 * </p>
 *
 * @author baggar
 * @since 2018-09-12
 */
public interface MoneyccMapper extends BaseMapper<Moneycc> {

}
