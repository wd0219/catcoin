package com.catcoin.finance.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.catcoin.finance.model.LogMailcc;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 * 用户消费账户流水表 Mapper 接口
 * </p>
 *
 * @author consume
 * @since 2018-09-03
 */
public interface LogMailccMapper extends BaseMapper<LogMailcc> {
    /**查询用户当日提币数量
     * @param userId
     * @return
     */
     BigDecimal selectMailccCountByUserId(@Param(value ="userId")Integer userId);
}
