package com.catcoin.finance.mapper;

import com.catcoin.finance.model.Account;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.catcoin.finance.model.AccountDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户账户表 Mapper 接口
 * </p>
 *
 * @author baggar
 * @since 2018-09-03
 */
public interface AccountMapper extends BaseMapper<Account> {
    public List<AccountDTO> selectAllList();

    public AccountDTO selectAccountDtoByUserId(@Param(value = "userId") Integer userId);
}
