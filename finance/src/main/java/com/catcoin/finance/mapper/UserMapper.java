package com.catcoin.finance.mapper;

import com.catcoin.finance.model.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author caody
 * @since 2018-08-31
 */
public interface UserMapper extends BaseMapper<User> {

}
