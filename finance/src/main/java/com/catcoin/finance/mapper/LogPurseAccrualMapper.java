package com.catcoin.finance.mapper;

import com.catcoin.finance.model.LogPurseAccrual;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 理财钱包利息流水 Mapper 接口
 * </p>
 *
 * @author caody
 * @since 2018-09-03
 */
public interface LogPurseAccrualMapper extends BaseMapper<LogPurseAccrual> {


    List<Map<String,String>> queryPurseAccrualLog(@Param(value = "userId")Integer userId, @Param(value = "purseAccrualType") Integer purseAccrualType, @Param(value = "type") Integer type);
}
