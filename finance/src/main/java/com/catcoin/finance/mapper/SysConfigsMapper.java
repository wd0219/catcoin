package com.catcoin.finance.mapper;

import com.catcoin.finance.model.SysConfigs;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 系统配置表 Mapper 接口
 * </p>
 *
 * @author consume
 * @since 2018-09-04
 */
public interface SysConfigsMapper extends BaseMapper<SysConfigs> {

}
