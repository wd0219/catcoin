package com.catcoin.finance.util;


import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 */
@SuppressWarnings("all")
public class StringUtils {

	/**
	 * List去重
	 * @param list
	 * @return
	 */
	public static List removeDuplicate(List list) {
		HashSet h = new HashSet(list);
		list.clear();
		list.addAll(h);
		return list;
	}
	
	/**
     * 生成订单号
     * @param sign
	 * 猫币余额转入1
	 * 理财钱包提币到消费2
	 * 理财钱包提币到猫币余额3
	 * 理财钱包本金生成理财钱包利息 4
	 * 理财钱包本金清空 5
     * @return
     */
    public static String getOrderIdByTime(String sign) {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
        String newDate=sdf.format(new Date());
        String result="";
        Random random=new Random();
        for(int i=0;i<5;i++){
            result+=random.nextInt(10);
        }
        return sign+newDate+result;
    }



	/**
	 * 是否为空判断(trim)
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isBlank(String str) {
		return str == null || "".equals(str.trim());
	}
	
	/**
	 * 是否为空判断(trim)
	 * 
	 * @param obj
	 * @return
	 */
    public static boolean isBlank(Object obj){
        if (obj == null)
            return true;
        if (obj instanceof CharSequence)
            return ((CharSequence) obj).toString().trim().equals("");
        if (obj instanceof Collection)
            return ((Collection) obj).isEmpty();
        if (obj instanceof Map)
            return ((Map) obj).isEmpty();
        if (obj instanceof Object[]) {
            Object[] object = (Object[]) obj;
            if (object.length == 0) {
                return true;
            }
            boolean empty = true;
            for (int i = 0; i < object.length; i++) {
                if (!isEmpty(object[i])) {
                    empty = false;
                    break;
                }
            }
            return empty;
        }
        return false;
    }
    
	/**
	 * 是否为空判断(无trim)
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
			return str == null || "".equals(str);
	}

	
	/**
	 * 是否为空判断
	 * 
	 * @param obj
	 * @return
	 */
    public static boolean isEmpty(Object obj){
        if (obj == null)
            return true;
        if (obj instanceof CharSequence)
            return ((CharSequence) obj).length() == 0;
        if (obj instanceof Collection)
            return ((Collection) obj).isEmpty();
        if (obj instanceof Map)
            return ((Map) obj).isEmpty();
        if (obj instanceof Object[]) {
            Object[] object = (Object[]) obj;
            if (object.length == 0) {
                return true;
            }
            boolean empty = true;
            for (int i = 0; i < object.length; i++) {
                if (!isEmpty(object[i])) {
                    empty = false;
                    break;
                }
            }
            return empty;
        }
        return false;
    }

	/**
	 * 字段串数组第一组不为空
	 * 
	 * @param ss
	 * @return
	 */
	public static boolean firstNotEmpty(final String[] ss) {
		return ss != null && ss.length > 0 && ss[0] != null
				&& ss[0].length() > 0;
	}

	/**
	 * 将Object型转换为字符串
	 * 
	 * @param
	 * @return
	 */
	public static String valueOf(Object o) {
		if (o == null) {
			return null;
		}
		String s = null;
		if (o instanceof Number) {
			s = String.valueOf(o);
		} else {
			s = o.toString();
		}
		return s;
	}

	/**
	 * 取数组中的第一组值
	 * 
	 * @param obj
	 * @return
	 */
	public static String getFirstString(Object obj) {
		if (obj == null) {
			return "";
		}
		String s = null;
		if (obj instanceof String[]) {
			String[] ss = (String[]) obj;
			s = ss[0];
		} else if (obj instanceof String) {
			s = (String) obj;
		} else {
			s = obj.toString();
		}
		return s;
	}

	/**
	 * 获取数组的第一个元素，并且作了字符串null、""的判断，这两种情况都处理为null
	 * 
	 * @param obj
	 * @return
	 */
	public static String getFirstStr(Object obj) {
		if (obj == null) {
			return null;
		}
		String tmp = null;
		if (obj instanceof String[]) {
			String[] ss = (String[]) obj;
			tmp = ss[0];
		} else if (obj instanceof String) {
			tmp = (String) obj;
		}
		if ("".equals(tmp)) {
			tmp = null;
		}
		return tmp;
	}

	/**
	 * 将Null处理为空字符串
	 * 
	 * @param str
	 * @return
	 */
	public static String toEmpty(String str) {
		if (str == null) {
			return "";
		}
		return str;
	}

	/**
	 * 删除最后的逗号
	 */
	public static StringBuilder removeLastChar(String sb) {
		if (sb.trim().endsWith(",")) {
			sb = sb.substring(0, sb.lastIndexOf(","));
		}
		return new StringBuilder(sb);
	}

	/**
	 * 去掉连接线的UUID
	 * 
	 * @return
	 */
	public static String getUuid() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	/**
	 * 判断字符串是否为数字
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(str);
		return isNum.matches();
	}

	/**
	 * 删除字符串中的空白
	 */
	public static String removeBlank(String sb) {
		sb = sb.replaceAll("\\s*", "");
		return sb;
	}


}
